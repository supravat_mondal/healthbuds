<?php

/**
 * MagPassion_Ajaxbookmarks extension
 * 
 * @category   	MagPassion
 * @package		MagPassion_Ajaxbookmarks
 * @copyright  	Copyright (c) 2014 by MagPassion (http://magpassion.com)
 * @license	http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


include_once 'app/Mage.php';
Mage::app();
Mage::getSingleton('core/session', array('name' => 'frontend'));
Mage::app()->getRequest()->setParam('form_key', $key);
$_layout = Mage::getSingleton('core/layout');
$itemwidth = Mage::helper('ajaxbookmarks/ajaxbookmark')->getproductwidth();
$itemheigth = Mage::helper('ajaxbookmarks/ajaxbookmark')->getproductheight();

$mp_currenturl = Mage::app()->getRequest()->getParam('curl');

$mp_baseurl = Mage::getBaseUrl(); 

$mp_baseurl = str_replace("/index.php", "", $mp_baseurl); 

$search = base64_encode($mp_baseurl.'mp_ajaxbm.php?curl='.$mp_currenturl);

$content = '';
try{	
	$cookies = explode(';', $_SERVER['HTTP_COOKIE']);
	$book_marks = '';
	foreach ($cookies as $c) {
		if (strpos($c, "book_marks=") !== false) {
			$book_marks = $c;
			break;
		}
	}
	if ($book_marks) {
		$book_marks = substr($book_marks, 12);
		$book_marks = explode("%2C", $book_marks);

        if (count($book_marks) > 0 && $book_marks[0]) {
            foreach ($book_marks as $bid) 
            if ($bid) {

                $_product = Mage::getModel('catalog/product')->load($bid);
				if ($_product && $_product->getId() > 0) {
	                $_pid = $_product->getId();
                   
	                $_purl = str_replace ("mp_ajaxbm.php/", "", $_product->getProductUrl(false));
	                
	                $content .= '<li class="item" style="width:'.$itemwidth.'px; height:'.$itemheigth.'px;">';
                    if (Mage::helper('ajaxbookmarks/ajaxbookmark')->getshowimage() == 1) {
                        $imgwidth = Mage::helper('ajaxbookmarks/ajaxbookmark')->getimagetwidth();
                        $imgheigth = Mage::helper('ajaxbookmarks/ajaxbookmark')->getimageheight();
                        $content .= '<a href="'.$_purl.'" class="product-image">';
                        $content .= '<img src="'.Mage::helper('catalog/image')->init($_product, 'small_image')->keepTransparency(true)->resize($imgwidth, $imgheigth).'" width="'.$imgwidth.'" height="'.$imgheigth.'"/>';
                        $content .= '</a>';
                    }
	                
	                $content .= '<div onclick="un_book_mark_item_on_bookmark_page(this,'. $_pid .')" class="mp_sticky active_sticky" id="un'. $_pid .'"></div>';
                    if (Mage::helper('ajaxbookmarks/ajaxbookmark')->getshowname() == 1) {
                        $content .= '<h2 class="product-name"><a href="'.$_purl.'" title="'.Mage::helper('core')->stripTags($_product->getName(), null, true).'">';
                        $content .= Mage::helper('catalog/output')->productAttribute($_product, $_product->getName(), 'name');
                        $content .= '</a></h2>';
                    }
                    
                    if (Mage::helper('ajaxbookmarks/ajaxbookmark')->getshowreview() == 1) {
                        $content .= str_replace("mp_ajaxbm.php/", "", $_layout->createBlock('review/helper')->getSummaryHtml($_product, 'short', false));
                    }
                    
                    if (Mage::helper('ajaxbookmarks/ajaxbookmark')->getshowprice() == 1) {
                        $content .= '<span class="price"><b>'.Mage::helper('core')->currency($_product->getFinalPrice(),true,false).'</b></span>';
                    }
                    
                    if (Mage::helper('ajaxbookmarks/ajaxbookmark')->getshowaddtocart() == 1 || Mage::helper('ajaxbookmarks/ajaxbookmark')->getshowaddtolink() == 1) {
                        $content .= '<div class="actions">';
                        if (Mage::helper('ajaxbookmarks/ajaxbookmark')->getshowaddtocart() == 1) {
                            if($_product->isSaleable()) {
                                $_addToCartUrl = str_replace ("mp_ajaxbm.php/", "", Mage::helper('checkout/cart')->getAddUrl($_product));
                                
                                $content .=  '<p><button type="button" title="Add to Cart" class="button btn-cart" onclick="setLocation(\''.$_addToCartUrl.'\')"><span><span>Add to Cart</span></span></button></p>';
                            }
                            else {
                                $content .= '<p class="availability out-of-stock"><span>Out of stock</span></p>';
                            }
                        }

                        if (Mage::helper('ajaxbookmarks/ajaxbookmark')->getshowaddtolink() == 1) {
                            $content .= '<ul class="add-to-links">';
                            if (Mage::helper('wishlist')->isAllow()) {
                                
                                
                                $_addtowishlist = str_replace ($search, $mp_currenturl, Mage::helper('wishlist')->getAddUrl($_product));
                               
                                $content .= '<li><a href="'.$_addtowishlist.'" class="link-wishlist">Add to Wishlist</a></li>';
                            }
                            if ($_compareUrl=Mage::helper('catalog/product_compare')->getAddUrl($_product, false)) {
                                
                                
                                
                                $new_addtocomparelink = str_replace($search,$mp_currenturl,$_compareUrl);
                                
                                
                                $content .= '<li><span class="separator">|</span> <a href="'.$new_addtocomparelink.'" class="link-compare">Add to Compare</a></li>';
                                
                               
                            }
                            $content .= '</ul>';
                        }
                        $content .= '</div>'; 
                    }
	                $content .= '</li>'; 
				}
				
            }
        }
	}
    	
	if ($content == '')	$content = 'There are no saved data';
}
catch (Exception $e) {
}

echo $content;

?>
