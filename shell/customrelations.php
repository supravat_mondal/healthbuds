<?php
/**
 * Intellimage
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to mauricioprado00@gmail.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade your
 * Intellimage extension to newer versions in the future. 
 * If you wish to customize your Intellimage extension to your
 * needs please refer to mauricioprado00@gmail.com for more information.
 *
 * @package     Intellimage_CustomRelations
 * @author      Hugo Mauricio Prado Macat
 * @copyright   2013
 * @email       mauricioprado00@gmail.com
 * @license     http://www.opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */



require_once 'abstract.php';

/**
 * Intellimage Custom Relations Shell Script
 *
 * @category    Intellimage
 * @package     Intellimage_CustomRelations
 * @author      Hugo Mauricio Prado Macat <mauricioprado00@gmail.com>
 */
class Mage_Shell_CustomRelations extends Mage_Shell_Abstract
{

    public function run()
    {
        $import = Mage::helper('customRelations/import');
        $action = $this->getArg('action');
        $filename = $this->getArg('filename');
        $mode = $this->getArg('mode');
        $source = $this->getArg('source');
        $import->setVerboseMode(true);

        try {
            switch ($action) {
                case 'import':

                    $filename = $filename ? $filename : null;
                    $mode = $mode ? $mode : "replace";
                    echo "Importing in mode: "  . $mode . PHP_EOL;

                    $import->runImport($filename, $mode === 'append', $source === 'ftp');
                    echo  "Import finished" . PHP_EOL;
                    break;
                
                default:
                    echo $this->usageHelp();
                    break;
            }
        } catch(Exception $e) {
            echo "an error ocurred: \n" . $e->getMessage();
            Mage::log($e->getMessage(), Zend_Log::ERR);

        }

        echo PHP_EOL;
    }


    /**
     * Retrieve Usage Help Message
     *
     */
    public function usageHelp()
    {
        return <<<USAGE
Usage:  php -f customrelations.php -- [options]

  --action <action>             Valid actions are "import"
  --mode <mode>                 Set the mode option. "replace" is set by default.
  --filename <filename>         The file must be located at var/import
  --source <source>             By default the source is "directory", but you can specify "ftp" 
                                to download from the configured ftp connection at 
                                    system > configuration > advanced > system > custom relations import

  <mode>        for import action, mode is set to "replace"  by default. Another value is "append". 
                "append" mode:  the previous relations of each product in the csv will be preserved, 
                                otherwise it will be cleared before adding them 
                                Example, if in the csv there is only an "accesory" for a product, 
                                then after running the import that product will have that only relation)

USAGE;
    }
}

$shell = new Mage_Shell_CustomRelations();
$shell->run();
