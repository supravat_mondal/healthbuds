/**
 * MagPassion_Ajaxbookmarks extension
 * 
 * @category   	MagPassion
 * @package		MagPassion_Ajaxbookmarks
 * @copyright  	Copyright (c) 2014 by MagPassion (http://magpassion.com)
 * @license	http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

	var book_marks 	= '';
	var book_marks_arr	= [];
	var total_book_marks = 0;
	
	var html_bookmark = '';
    var expires_time = 7;
    //alert (base_url + ' ' + expires_time);
    
	book_marks = jQuery.cookie('book_marks');//Các tin đã lưu
	
    if(!book_marks)
        book_marks = '';

    if(book_marks != '')
    {
        book_marks_arr = book_marks.split(',');
        total_book_marks = parseInt(book_marks_arr.length);
    }
	
	
	jQuery(document).ready(function(){
		if(typeof(book_marks) != 'undefined' && book_marks != '') {
			jQuery( ".mp_sticky" ).each(function() {
				if (in_array( jQuery(this).attr('id'), book_marks_arr )) {
					jQuery(this).removeClass("not_sticky");
					jQuery(this).addClass("active_sticky");
				}
			});
			
		}
		
		reloadBM(0);
        
        jQuery( "#ajaxbookmark" ).click(function() {
            jQuery('.magpassion_bookmark_page').css('display','inline');
            loadBookmarkProduct();
            return false;
        });
        
	});
	
	function book_mark_item(o, ad_id)
	{
        //alert(book_marks);
		if(typeof(book_marks) != 'undefined' && book_marks != '')
		{
            //alert('11111');
			//Begin Trường hợp Bỏ đánh dấu tin
			if (jQuery("#"+ o.id).hasClass('active_sticky')==true || jQuery("#"+ o.id).hasClass('unbookmark')==true) {
				jQuery("#"+ad_id).removeClass("active_sticky");
				jQuery("#"+ad_id).addClass("not_sticky");
				

				var strIdBM = "";

				for(i = 0; i < book_marks_arr.length; i++)
				{
					if(ad_id !== book_marks_arr[i])
					{
						strIdBM += ","+book_marks_arr[i];
					}
				}
				//alert(strIdBM);
				if (strIdBM) {
					book_marks = strIdBM.substring(1, strIdBM.length);
					book_marks_arr = book_marks.split(',');
				}
				else {
					book_marks = "";
					book_marks_arr	= [];
				}
				total_book_marks = parseInt(book_marks_arr.length);
				
				jQuery.cookie('book_marks', book_marks);
				
				reloadBM(ad_id);
				
				return;
			}
			//End Trường hợp Bỏ đánh dấu tin

			if (ad_id && in_array(ad_id, book_marks_arr)) return;
		}
        
        //alert('22222');
		//Đánh dấu
		book_marks = (book_marks == '') ? ad_id : ad_id+','+book_marks;
		book_marks_arr[book_marks_arr.length] = ad_id;

		total_book_marks = parseInt(book_marks_arr.length);
        //alert(book_marks);
		//set cookie theo ngay
		jQuery.cookie('book_marks', book_marks);

		//Thực hiện gọi server get data và tăng số lượng tin đánh dấu
		//Đánh dấu tin vừa bookmark
		if (document.getElementById(ad_id) != null)
			document.getElementById(ad_id).className="mp_sticky active_sticky";
		
		reloadBM(ad_id);
		
	}
	
	function un_book_mark_item(o, ad_id)
	{
		jQuery("#"+ad_id).removeClass("active_sticky");
		jQuery("#"+ad_id).addClass("not_sticky");

		var strIdBM = "";

		for(i = 0; i < book_marks_arr.length; i++)
		{
			if(parseInt(ad_id) !== parseInt(book_marks_arr[i]))
			{
				strIdBM += ","+book_marks_arr[i];
			} 
		}
		if (strIdBM) {
			book_marks = strIdBM.substring(1, strIdBM.length);
			book_marks_arr = book_marks.split(',');
		}
		else {
			book_marks = "";
			book_marks_arr	= [];
		}
		total_book_marks = parseInt(book_marks_arr.length);
		
		jQuery.cookie('book_marks', book_marks);
		
		reloadBM(ad_id);
	}
    
    function un_book_mark_item_on_bookmark_page(o, ad_id)
	{
		un_book_mark_item(o, ad_id);
        loadBookmarkProduct();
	}
	
	function in_array(value, array){
		value = parseInt(value); 
		for (i = 0; i < array.length / 2 + 1 ; i++)
			if( (2*i < array.length && parseInt(array[2*i]) === value) || ( i > 0 && parseInt(array[2*i - 1]) === value ))
				return true;
		return false;
	}
	
	function reloadBM(ad_id) {
		//Đánh dấu tin vừa bookmark
		if (total_book_marks == 0)
		{
			jQuery('#count_bm').css("display", "none");
            jQuery('.ajaxbm-top-link').css("padding-right", "0px");
            jQuery('#ajaxbm-star').css("right", "0px");
		}
		else {
            jQuery('.ajaxbm-top-link').css("padding-right", "16px");
            jQuery('#ajaxbm-star').css("right", "16px");
			jQuery('#count_bm').css("display", "block");
			jQuery('#count_bm').html(total_book_marks);

		}
		
       
	}
    
    
    function closeBM(){
        jQuery('.magpassion_bookmark_page').css('display','none');
    }
    
    function clearAllBM(){
        jQuery( ".mp_sticky" ).each(function() {
            if (in_array( jQuery(this).attr('id'), book_marks_arr )) {
                jQuery(this).removeClass("active_sticky");
                jQuery(this).addClass("not_sticky");
            }
        });
        
        book_marks = '';
        book_marks_arr = [];
        total_book_marks = 0;
        reloadBM(0);
        jQuery.cookie('book_marks', '');
        closeBM();
    }

    