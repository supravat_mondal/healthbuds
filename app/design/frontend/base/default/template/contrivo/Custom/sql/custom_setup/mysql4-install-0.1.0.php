<?php 
$installer = $this;

$installer->startSetup();

$installer->getConnection()
	->addColumn(
		$this->getTable('webforms'),
		'enable_sms_service',
		'tinyint(1) NOT NULL DEFAULT "0" AFTER `code`'
	);

/*$installer->getConnection()
	->addColumn(
		$this->getTable('webforms'),
		'add_sms_header',
		'tinyint(1) NOT NULL DEFAULT "0" AFTER `code`'
	);*/

$installer->getConnection()
	->addColumn(
		$this->getTable('webforms'),
		'sms_mobile_number',
		'varchar(255) AFTER `code`'
	);

$installer->endSetup();
