<?php
class Contrivo_Custom_Model_Observer
{	
	protected $smsFieldsToSkipArr = array('subscribe','file','image','html','wysiwyg');
	protected $webFormSmsLog = 'webforms_sms.log';
    protected $defaultSMSText = "Thanks for your submission. We will contact you soon.";

    protected $fields;
    protected $fieldsIdAndValArr;
    protected $accountCreatedSuccessMsg = 'Your account has been created successfully and a password reset email has been sent to your email address.';

    const XML_PATH_CUSTOMER_NEW_ACCOUNT_EMAIL = 'webforms/email_template/customer_new_account';
    const XML_PATH_CUSTOMER_RESET_PASSWORD = 'webforms/email_template/customer_reset_password';

	public function addWebFormSmsTabs(Varien_Event_Observer $observer)
    {
    	$tabsObj = $observer->getEvent()->getData('tabs');

    	$tabsObj->addTab('form_sms',array(
            'label' => Mage::helper('webforms')->__('SMS Settings'),
            'title' => Mage::helper('webforms')->__('SMS Settings'),
            'content' => $tabsObj->getLayout()->createBlock('custom/adminhtml_webforms_edit_tab_sms')->toHtml(),
            'after' => 'form_email',
        ));
    }

    public function sendSmsOnWebFormSubmit(Varien_Event_Observer $observer)
    {
    	$resultObj = $observer->getEvent()->getData('result');
    	$webformObj = $observer->getEvent()->getData('webform');
    	
    	$postData = Mage::app()->getRequest()->getPost();
    	
    	$new_result = true;
        if (!empty($postData['result_id'])) {
            $new_result = false;
        }

        if ($new_result) {
            $this->fieldsIdAndValArr = $resultObj->getData('field'); //fields ID and thier values

            $this->fields = Mage::getModel('webforms/fields')
                        ->setStoreId(Mage::app()->getStore()->getId())
                        ->getCollection()
                        ->addFilter('webform_id', $webformObj->getId()); //get details of all the fields of an webform

            $this->registerCustomer($webformObj);

            if($webformObj->getData('enable_sms_service')){
                $this->sendSms($webformObj);
            }        	            
        }
    }

    private function registerCustomer($webformObj){
        try{
            $customerData = array('email' => '', 'firstname' => '', 'lastname' => '');
            $canRegisterCustomer = false;

            foreach($this->fields as $field)
            {    

               if($field->getType() == 'registration')   
                {
                    if(isset($this->fieldsIdAndValArr[$field->getData('id')])){
                        $canRegisterCustomer = $this->fieldsIdAndValArr[$field->getData('id')];
                        continue;
                    }                    
                }

                switch ($field->getData('code')) {
                    case 'email':
                        $customerData['email'] = $this->fieldsIdAndValArr[$field->getData('id')];
                    break;

                    case 'firstname':
                        $customerData['firstname'] = $this->fieldsIdAndValArr[$field->getData('id')];
                    break;

                    case 'lastname':
                        $customerData['lastname'] = $this->fieldsIdAndValArr[$field->getData('id')];
                    break;
                    
                    default:
                        continue;
                }
            }

            if($canRegisterCustomer)
            {
                if(!empty($customerData['firstname']) &&
                   !empty($customerData['lastname']) &&
                   Zend_Validate::is($customerData['email'], 'EmailAddress'))
                {
                    $pwd_length = 7; //auto-generated password length

                    $customer = Mage::getModel('customer/customer');
                    $customer->setWebsiteId(Mage::app()->getWebsite()->getId());
                    $customer->setStore(Mage::app()->getStore());  
                    $customer->loadByEmail($customerData['email']);

                    //if customer is not already registered
                    if(!$customer->getId()) {       
                      $customer = Mage::getModel('customer/customer');

                      $customer->setWebsiteId(Mage::app()->getWebsite()->getId());
                      $customer->setStore(Mage::app()->getStore());  
                      $customer->setEmail($customerData['email']); 
                      $customer->setFirstname($customerData['firstname']);
                      $customer->setLastname($customerData['lastname']);
                      $customer->setPassword($customer->generatePassword($pwd_length));     
                      $customer->setGroupId(1);  //assigned to general user group
                      
                      Mage::register('webformscrf_customer_after_save', true); //This is done to halt listening of event "customer_save_after" in webform module
                      $customer->save();
                      $to = array('email' => $customerData['email'] , 'name' => $customerData['firstname'] .' '. $customerData['lastname']);
                      
                      //add password reset link to customer new account email
                      $newResetPasswordLinkToken =  Mage::helper('customer')->generateResetPasswordLinkToken();
                      $customer->changeResetPasswordLinkToken($newResetPasswordLinkToken);

                      $this->sendNewAccountEmail($to,$customer); //send confirmation email to customer  
                        
                      //Once customer is registered, send password reset mail too
                      /*$newResetPasswordLinkToken =  Mage::helper('customer')->generateResetPasswordLinkToken();
                      $customer->changeResetPasswordLinkToken($newResetPasswordLinkToken);
                      $this->sendPasswordResetEmail($to,$customer); //send password reset email to customer*/

                      $successTxt = $webformObj->getSuccessText().$this->accountCreatedSuccessMsg;
                      $webformObj->setSuccessText($successTxt);
                    }else{
                        Mage::log("Customer with email address(".$customerData['email'].") is already registered.", null, $this->webFormSmsLog); 
                    }                    
                    
                }else{
                    Mage::log("Data required to register customer is not complete.".json_encode($customerData), null, $this->webFormSmsLog); 
                }
            }            
        }catch (Exception $e) {
            Mage::log($e->__toString(), null, $this->webFormSmsLog);
        }  
        catch (Mage_Core_Exception $e) {
            Mage::log($e->__toString(), null, $this->webFormSmsLog);
        }  
    }

    private function sendNewAccountEmail($to,$customer, $templateConfigPath = self::XML_PATH_CUSTOMER_NEW_ACCOUNT_EMAIL)
    {
        if (!$to) return;
 
        $translate = Mage::getSingleton('core/translate');
        /* @var $translate Mage_Core_Model_Translate */
        $translate->setTranslateInline(false);
 
        $mailTemplate = Mage::getModel('core/email_template');
        /* @var $mailTemplate Mage_Core_Model_Email_Template */
 
        $template = Mage::getStoreConfig($templateConfigPath, Mage::app()->getStore()->getId());

        $recepientArr = array('email' => Mage::getStoreConfig('trans_email/ident_sales/email',Mage::app()->getStore()->getId()),
                              'name' => Mage::getStoreConfig('trans_email/ident_sales/name',Mage::app()->getStore()->getId()));

        $mailTemplate->setDesignConfig(array('area'=>'frontend', 'store'=>Mage::app()->getStore()->getId()))
                     ->sendTransactional(
                            $template,
                            $recepientArr,
                            $to['email'],
                            $to['name'],
                                array(
                                    'customer'  => $customer
                                )
                        );
 
        $translate->setTranslateInline(true);
 
        return $this;
    }

    /*private function sendPasswordResetEmail($to,$customer, $templateConfigPath = self::XML_PATH_CUSTOMER_RESET_PASSWORD)
    {
        if (!$to) return;
 
        $translate = Mage::getSingleton('core/translate');

        $translate->setTranslateInline(false);
 
        $mailTemplate = Mage::getModel('core/email_template');
 
        $template = Mage::getStoreConfig($templateConfigPath, Mage::app()->getStore()->getId());
        
        $recepientArr = array('email' => Mage::getStoreConfig('trans_email/ident_sales/email',Mage::app()->getStore()->getId()),
                              'name' => Mage::getStoreConfig('trans_email/ident_sales/name',Mage::app()->getStore()->getId()));

        $mailTemplate->setDesignConfig(array('area'=>'frontend', 'store'=>Mage::app()->getStore()->getId()))
                     ->sendTransactional(
                            $template,
                            $recepientArr,
                            $to['email'],
                            $to['name'],
                                array(
                                    'customer'  => $customer
                                )
                        );
 
        $translate->setTranslateInline(true);
 
        return $this;
    }*/

    private function sendSms($webformObj)
    {
        try{

            $mobileNumbersArr = array();

            $mobileNumbers = trim($webformObj->getData('sms_mobile_number')); //fetch admin notification mobile number

            if($mobileNumbers != ''){
                $mobileNumbersArr = explode(",",$mobileNumbers);
            }                

            $msgTxt = '';                

            //Check if customer's mobile no. and country code are available or not
            $canSendSms = false;

            foreach($this->fields as $field)
            {
               //Fetch user number. User provided mobile number would be present in this field  
               if($field->getData('code') == 'contact_number')   
                {
                    $contact_number = $this->fieldsIdAndValArr[$field->getData('id')];
                }

                //Fetch country's short name code if present. Then fetch country calling code using country's short name code.
                if($field->getData('code') == 'country'){
                    $countryShortNameCode = $this->fieldsIdAndValArr[$field->getData('id')];

                    if(isset($countryShortNameCode) && $countryShortNameCode != ''){
                        $countryCallingCode = Mage::helper('custom')->getCountryCallingCode(strtolower($countryShortNameCode));
                    }                    
                }
            }

            //check if customer mobile no is available
            if(isset($contact_number) && $contact_number != ''){
                $canSendSms = true;

                //check if country code is avaialble, if so prepend country calling code in customer mobile number
                if(isset($countryCallingCode) && $countryCallingCode != ''){
                    $mobileNumbersArr[] = $countryCallingCode . $contact_number;
                }else{
                    $mobileNumbersArr[] = $contact_number;
                }
                
            }

            if(!$canSendSms){
                Mage::log("Mobile number is not provided by customer. Following details were entered in form(".$webformObj->getData('name')."):".print_r($this->fieldsIdAndValArr,true), null, $this->webFormSmsLog); 
                return;
            }

            if($canSendSms){
                //check if any sms template is set 
                if($smsTemplateId = $webformObj->getData('sms_template_id')){
                    $smsTemplate = Mage::getModel('core/email_template')->load($smsTemplateId);

                    //prepare sms variables array
                    $vars = array();

                    //set store object
                    $vars['store'] = Mage::app()->getStore();

                    //get and set customer object to variable
                    $customer = Mage::getSingleton('customer/session');
                    if($customer->isLoggedIn()){
                        $vars['customer'] = $customer->getCustomer();
                    }

                    //prepare form data object
                    $formDataObj = $this->prepareFormDataObj();
                    $vars['form'] = $formDataObj;

                    $msgTxt = $smsTemplate->getProcessedTemplate($vars);  //process template and get message content                      
                    
                }else{
                    $msgTxt = $this->defaultSMSText;
                }
            }

  
            if($msgTxt == ''){
                return;
            }

            $key = Mage::getStoreConfig('webforms/sms/api_key');
            $secret = Mage::getStoreConfig('webforms/sms/api_secret');
            $from = Mage::getStoreConfig('webforms/sms/sms_from');
           
            $message=  rawurlencode($msgTxt);

            $iClient = new Varien_Http_Client();

            foreach ($mobileNumbersArr as $mobileNumber) {
                $endpoint="https://rest.nexmo.com/sms/json?api_key=".$key."&api_secret=".$secret."&from=".$from."&to=".$mobileNumber."&text=".$message;
                $iClient->setUri($endpoint)
                        ->setMethod('POST')
                        ->setConfig(array(
                            'maxredirects' => 0,
                            'timeout' => 30,
                ));
                
                $response = $iClient->request();

                if ($response->getStatus() == '200') {
                    $responseBody = $response->getBody();
                    
                    //hardcoded response for testing
                   /* $responseBody = '{
                                "message-count": "1",
                                "messages": [{
                                    "to": "1234567890",
                                    "message-id": "0600000026D30C85",
                                    "status": "0",
                                    "remaining-balance": "1.98400000",
                                    "message-price": "0.00800000",
                                    "network": "40410"
                                }]
                            }';*/

                    $body = json_decode($responseBody,true);
                    Mage::log(json_encode($body), null, $this->webFormSmsLog);  //json_encoded again to remove pretty json format

                    if($body['messages']['0']['status']=='0'){
                      Mage::log("Message Send Successfully(".$message.')', null, $this->webFormSmsLog);     

                    }else{                      
                      Mage::log($body['messages']['0']['error-text'], null, $this->webFormSmsLog);    

                    }
                }else{
                  Mage::log("Response Error", null, $this->webFormSmsLog);   
                }
            }
         }catch (Exception $e) {
            Mage::log($e->__toString(), null, $this->webFormSmsLog);
        }catch(Mage_Core_Exception $e){
            Mage::log($e->__toString(), null, $this->webFormSmsLog);
        }
    }

    private function prepareFormDataObj(){
        $formDataObj = new Varien_Object();
        $fields = $this->fields;
        $fieldsIdAndValArr = $this->fieldsIdAndValArr;

        foreach($fields as $field)
        {
            $fieldId = $field->getData('id');

            if(isset($fieldsIdAndValArr[$fieldId]) && 
                $fieldsIdAndValArr[$fieldId] != '' &&
                !in_array($field->getData('type'),$this->smsFieldsToSkipArr))
            {
                if(is_array($fieldsIdAndValArr[$fieldId]))
                {
                    if(count($fieldsIdAndValArr[$fieldId]))
                    {
                        $formDataObj->setData($field->getData('code'), implode(",", $fieldsIdAndValArr[$fieldId]));
                    }

                }
                elseif($field->getData('code') == 'star_rate' && is_array($field->getData('value')))
                {
                     $starRateFieldValArr = $field->getData('value');

                     if(isset($starRateFieldValArr['stars_max']) && $starRateFieldValArr['stars_max'] != '')
                     {
                         $formDataObj->setData($field->getData('code'), ($fieldsIdAndValArr[$fieldId] . ' (out of ' . $starRateFieldValArr['stars_max'].')'));
                     }else{
                         $formDataObj->setData($field->getData('code'), ($fieldsIdAndValArr[$fieldId] . ' (out of 5)')); // 5 is default value
                     }                           
                }else
                {          
                    $formDataObj->setData($field->getData('code'), $fieldsIdAndValArr[$fieldId]);
                }
            }
        }

        return $formDataObj;
    }

    public function addWebFormsFieldType(Varien_Event_Observer $observer)
    {
        $fieldTypesObj = $observer->getEvent()->getData('types');
        $fieldTypesObj->setData('registration',Mage::helper('webforms')->__('Registration'));
    }

    public function addWebFormsFieldTypeFrontendTemplate(Varien_Event_Observer $observer){
        
        $fieldObj = $observer->getEvent()->getData('field');            
        $field_type = $fieldObj->getType();

        if($field_type == 'registration' || $field_type == 'date' || $field_type == 'datetime')
        {
            $filter = $fieldObj->getFilter();

            $field_id = "field" . $fieldObj->getId();
            $field_name = "field[" . $fieldObj->getId() . "]";
            $field_value = @$filter->filter($fieldObj->getValue());
            $result = $fieldObj->getData('result');
            $customer_value = $result ? $result->getData('field_' . $fieldObj->getId()) : false;
            $fieldObj->setData('customer_value', $customer_value);
            
            $field_class = "input-text";
            $field_style = "";

            if ($field_type == 'file' || $field_type == 'image') {
                $field_class = "input-file";
            }
            if ($fieldObj->getRequired())
                $field_class .= " required-entry";
            if ($field_type == "email")
                $field_class .= " validate-email";
            if ($field_type == "number")
                $field_class .= " validate-number";

            if (!empty($field_value['number_min'])) {
                $field_class .= ' validate-field-number-min-' . $fieldObj->getId();
            }
            if (!empty($field_value['number_max'])) {
                $field_class .= ' validate-field-number-max-' . $fieldObj->getId();
            }

            if ($field_type == "url")
                $field_class .= " validate-url";
            if ($fieldObj->getCssClass()) {
                $field_class .= ' ' . $fieldObj->getCssClass();
            }
            if ($fieldObj->getData('validate_length_min') || $fieldObj->getData('validate_length_max')) {
                $field_class .= ' validate-length';
            }
            if ($fieldObj->getData('validate_length_min')) {
                $field_class .= ' minimum-length-' . $fieldObj->getData('validate_length_min');
            }
            if ($fieldObj->getData('validate_length_max')) {
                $field_class .= ' maximum-length-' . $fieldObj->getData('validate_length_max');
            }
            if ($fieldObj->getData('validate_regex')) {
                $field_class .= ' validate-field-' . $fieldObj->getId();
            }
            if ($fieldObj->getRequired() && $fieldObj->getHint()) {
                $field_class .= ' validate-field-hint-' . $fieldObj->getId();
            }
            if ($fieldObj->getCssStyle()) {
                $field_style = $fieldObj->getCssStyle();
            }
            $tinyMCE = false;
            $showTime = false;
            $calendar = false;

            $config = array(
                'field' => $fieldObj,
                'field_id' => $field_id,
                'field_name' => $field_name,
                'field_class' => $field_class,
                'field_style' => $field_style,
                'result' => $result,
                'show_time' => 'false',
                'customer_value' => $customer_value,
                'template' => 'webforms/fields/text.phtml'
            );

            if($field_type == 'registration')
            {
                $config['field_class'] = $fieldObj->getCssClass();
                $config['template'] = 'contrivo/webforms/registration.phtml'; //fieldtype template
                $config['label'] = $field_value['registration_label'];  //fieldtype label            
            }elseif($field_type == 'date' || $field_type == 'datetime')
            {
                $config['template'] = 'contrivo/webforms/date.phtml';
            }                      

            $layout = Mage::app()->getLayout();

            $html = $layout->createBlock('core/template', $field_name, $config)->toHtml();

            if ($fieldObj->getData('validate_regex')) {
                $flags = array();

                $regexp = trim($fieldObj->getData('validate_regex'));

                preg_match('/\/([igmy]{1,4})$/', $regexp, $flags);

                // set regex flags
                if (!empty($flags[1])) {
                    $flags = $flags[1];
                    $regexp = substr($regexp, 0, strlen($regexp) - strlen($flags));
                } else {
                    $flags = '';
                }

                if (substr($regexp, 0, 1) == '/' && substr($regexp, strlen($regexp) - 1, strlen($regexp)) == '/')
                    $regexp = substr($regexp, 1, -1);
                $regexp = str_replace('\\','\\\\',$regexp);

                $validate_message = trim(str_replace("'", "\'", $fieldObj->getData('validate_message')));
                $html .= "<script>Validation.add('validate-field-{$fieldObj->getId()}','{$validate_message}',function(v){var r = new RegExp('{$regexp}','{$flags}');return Validation.get('IsEmpty').test(v) || r.test(v);})</script>";
            }

            if ($fieldObj->getRequired() && $fieldObj->getHint()) {
                $validate_message = Mage::helper('core')->__('This is a required field.');
                $hint = trim(str_replace("'", "\'", $fieldObj->getHint()));
                $html .= "<script>Validation.add('validate-field-hint-{$fieldObj->getId()}','{$validate_message}',function(v){return v.trim() != '{$hint}';})</script>";
            }

            if ($field_type == 'number') {
                if (!empty($field_value['number_min'])) {
                    $validate_message = Mage::helper('webforms')->__('Minimum value is %s', $field_value['number_min']);
                    $html .= "<script>Validation.add('validate-field-number-min-{$fieldObj->getId()}','{$validate_message}',function(v){return v >= {$field_value['number_min']};})</script>";
                }
                if (!empty($field_value['number_max'])) {
                    $validate_message = Mage::helper('webforms')->__('Maximum value is %s', $field_value['number_max']);
                    $html .= "<script>Validation.add('validate-field-number-max-{$fieldObj->getId()}','{$validate_message}',function(v){return v <= {$field_value['number_max']};})</script>";
                }
            }


            // activate tinyMCE
            if ($tinyMCE && !Mage::registry('tinyMCE')) {
                Mage::register('tinyMCE', true);
                $tiny_mce = $layout->createBlock('core/template', 'tinyMCE', array('template' => 'webforms/scripts/tiny_mce.phtml'));
                $html .= $tiny_mce->toHtml();
            }

            // activate calendar
            if ($calendar && !Mage::registry('calendar')) {
                Mage::register('calendar', true);
                $calendar_block = $layout->createBlock('core/html_calendar', 'calendar_block', array
                (
                    'as' => 'calendar',
                    'template' => 'page/js/calendar.phtml'
                ));
                $html .= $calendar_block->toHtml();
            }

            $htmlObj = $observer->getEvent()->getData('html_object');
            $htmlObj->setHtml($html);
        }

    }

    public function modfiyWebFormsFieldTypeValueRepresentationCustomer(Varien_Event_Observer $observer){
        $fieldObj = $observer->getEvent()->getData('field');
        $valueObj = $observer->getEvent()->getData('value');
        $resultObj = $observer->getEvent()->getData('result');

        if($fieldObj->getType() == 'registration'){  
           $value = $resultObj->getData('field_' . $fieldObj->getId());

            if ($value){
                $value = Mage::helper('core')->__('Yes');
            }else{
                $value = Mage::helper('core')->__('No');                    
            }
            
            $valueObj->setData('html',$value);
        }
    }

    public function modfiyWebFormsFieldTypeValueRepresentationGrid(Varien_Event_Observer $observer){
        $fieldObj = $observer->getEvent()->getData('field');
        $configObj = $observer->getEvent()->getData('config');

        if ($fieldObj->getType() == 'registration') {
            $config = array();
            $config['type'] = 'options';
            $config['renderer'] = false;
            $config['options'] = array(
                0 => Mage::helper('adminhtml')->__('No'),
                1 => Mage::helper('adminhtml')->__('Yes'),
            );

            $configObj->setData('type',$config['type']);
            $configObj->setData('renderer',$config['renderer']);
            $configObj->setData('options',$config['options']);
        }
    }

    public function modfiyWebFormsFieldTypeValueRepresentationEdit(Varien_Event_Observer $observer){
        $formObj = $observer->getEvent()->getData('form');  //Not used in this method. Just for developer's knowledge
        $fieldsetObj = $observer->getEvent()->getData('fieldset'); //Not used in this method. Just for developer's knowledge
        $fieldObj = $observer->getEvent()->getData('field');
        $configObj = $observer->getEvent()->getData('config');

        if ($fieldObj->getType() == 'registration') {
            $config = array();
            $config['type'] = 'hidden';        

            $configObj->setData('type',$config['type']);
        }
    }
}   