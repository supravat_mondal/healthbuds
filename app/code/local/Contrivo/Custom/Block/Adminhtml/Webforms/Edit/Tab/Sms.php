<?php
class Contrivo_Custom_Block_Adminhtml_Webforms_Edit_Tab_Sms
    extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $renderer = $this->getLayout()->createBlock('webforms/adminhtml_element_field');
        $form->setFieldsetElementRenderer($renderer);
        $form->setFieldNameSuffix('form');
        $form->setDataObject(Mage::registry('webforms_data'));

        $this->setForm($form);

        $fieldset = $form->addFieldset('sms_settings', array(
            'legend' => Mage::helper('webforms')->__('SMS Settings')
        ));

        $fieldset->addField('enable_sms_service', 'select', array(
            'label'  => Mage::helper('webforms')->__('Enable'),
            'title'  => Mage::helper('webforms')->__('Enable'),
            'name'   => 'enable_sms_service',
            'values' => Mage::getModel('adminhtml/system_config_source_yesno')->toOptionArray(),
        ));

        $template = $fieldset->addField('sms_template_id', 'select', array(
            'label'    => Mage::helper('webforms')->__('SMS template'),
            'title'    => Mage::helper('webforms')->__('SMS template'),
            'name'     => 'sms_template_id',
            'required' => false,
            'note'     => Mage::helper('webforms')->__('Set SMS template. If default message is set then message "Thanks for your submission. We will contact you soon." will be sent.'),
            'values'   => Mage::helper('custom')->getTemplatesOptions(),
        ));

        /*$fieldset->addField('add_sms_header', 'select', array(
            'label'  => Mage::helper('webforms')->__('Add header to the message'),
            'title'  => Mage::helper('webforms')->__('Add header to the message'),
            'name'   => 'add_sms_header',
            'note'   => Mage::helper('webforms')->__('Add header with Store Group, IP and other information to the message'),
            'values' => Mage::getModel('adminhtml/system_config_source_yesno')->toOptionArray(),
        ));*/

        $email = $fieldset->addField('sms_mobile_number', 'text', array(
            'label' => Mage::helper('webforms')->__('Admin notification mobile numbers'),
            'note'  => Mage::helper('webforms')->__('You can set multiple mobile numbers comma-separated'),
            'name'  => 'sms_mobile_number'
        ));

        Mage::dispatchEvent('webforms_adminhtml_webforms_edit_tab_sms_prepare_form', array('form' => $form, 'fieldset' => $fieldset));

        if (Mage::getSingleton('adminhtml/session')->getWebFormsData()) {
            $form->setValues(Mage::getSingleton('adminhtml/session')->getWebFormsData());
            Mage::getSingleton('adminhtml/session')->setWebFormsData(null);
        } elseif (Mage::registry('webforms_data')) {
            $form->setValues(Mage::registry('webforms_data')->getData());
        }

        return parent::_prepareForm();
    }
}