<?php 
$installer = $this;

$installer->startSetup();

$installer->getConnection()
	->addColumn(
		$this->getTable('webforms'),
		'sms_template_id',
		'int( 11 ) NOT NULL AFTER `sms_mobile_number`'
	);

$installer->endSetup();	
?>