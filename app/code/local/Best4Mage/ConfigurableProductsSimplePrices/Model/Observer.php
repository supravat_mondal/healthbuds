<?php

class Best4Mage_ConfigurableProductsSimplePrices_Model_Observer
{
	public function showOutOfStock($observer){
        if($_product = Mage::registry('current_product')){
			$_cpsp = Mage::helper('configurableproductssimpleprices');
			if($_cpsp->isEnable($_product) && (1*$_cpsp->showCpspStock($_product) !== 0)){
				Mage::helper('catalog/product')->setSkipSaleableCheck(true);
			}
		}
    }
}
