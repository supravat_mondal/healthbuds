<?php
class Myst_Doctorsearch_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
		$this->loadLayout();     
		$this->renderLayout();
    }
	public function postAction()
	{		
		$total_ids = $this->getRequest()->getParam('total_id');
		
		// exit;
		$t_ids = explode(",", $total_ids);
		$filter_ids = array();
		foreach($t_ids as $_id){
			$filter_ids[] = $_id;
		}
		// print_r($filter_ids);
		// exit;
		
		
		
		$idsStr = $this->getRequest()->getParam('id');

		$filter = array();
		if(!empty($idsStr)){
			$ids = explode(",", $idsStr);
			foreach ($ids as $id) {
				$filter[] = array(
					'attribute' => 'speciality',
					'finset'    => $id
				);
			}
		}
		
		$filterProduct = array();
		$filterProducts = array();
        $productCollection = Mage::getModel('catalog/product')->getCollection();
		$productCollection->addAttributeToFilter('entity_id', array('in' => $filter_ids));
		
		if (count($filter) > 0) {			
			$productCollection->addAttributeToFilter($filter);
			// $productCollection->addAttributeToFilter('attribute_set_id','10');
		}

		foreach($productCollection as $_product){
			$filterProduct[] = $_product->getId();
		}
		//print_r($filterProduct);
		//exit;
		$filterProducts = array_unique($filterProduct);
		// print_r($filterProducts);
		// exit;
		$this->loadLayout();
		$myBlock = $this->getLayout()->createBlock('core/template');
		$myBlock->setFilteredIds($filterProducts);
		$myBlock->setTemplate('catalog/product/list/filter_doctors.phtml');
		$myHtml =  $myBlock->toHtml();
		//echo $myHtml;
		//exit;
		return $this->getResponse()
			   ->setHeader('Content-Type', 'text/html')
			   ->setBody($myHtml);
		
	}
}