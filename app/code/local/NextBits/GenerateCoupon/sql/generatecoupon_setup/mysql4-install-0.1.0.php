<?php

$installer = $this;

$installer->startSetup();
$conn = $installer->getConnection();
$conn->addColumn($this->getTable('sales_flat_order'), 'automatic_generated_coupon', 'varchar(255) null');

$installer->endSetup(); 
