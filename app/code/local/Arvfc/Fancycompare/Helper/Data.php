<?php

class Arvfc_Fancycompare_Helper_Data extends Mage_Core_Helper_Abstract
{
	public function canAddToCompare($new_product) {
		$_items = Mage::helper('catalog/product_compare')->getItemCollection();

		// If no item is in compare collection
		// allow to add items
		if (!$_items->getSize()) {
			return true;
		}

		$all_categories = array();
		foreach($_items as $_index => $_item) {
			// var_dump($_item->getData('entity_id'));die;
			$product = Mage::getModel('catalog/product')
		                ->setStoreId(Mage::app()->getStore()->getId())
		                ->load($_item->getData('entity_id'));
			$all_categories[] = $this->getParentCategory($product);
		}

		$new_category = $this->getParentCategory($new_product);
		if (in_array($new_category, $all_categories)) {
			return true;
		}
		return false;
	}

	public function getParentCategory($product) {
        $category_ids = $product->getCategoryIds();
        if (is_array($category_ids)) {
            sort($category_ids);
            return $category_ids[0];
        }
        return null;
	}
}