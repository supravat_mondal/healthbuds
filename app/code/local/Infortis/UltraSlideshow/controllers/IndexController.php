<?php

class Infortis_UltraSlideshow_IndexController extends Mage_Core_Controller_Front_Action
{
	public function indexAction() {
		if($this->getRequest()->getPost()) {
			$_helper = Mage::helper('catalog/category');
			//if(isset($_POST['redirect_url'])) {
			$cat = Mage::getResourceModel('catalog/category_collection')->addFieldToFilter('name', $this->getRequest()->getPost('redirect_url') );
			//$_category = Mage::getModel('catalog/category')->load($cat->getFirstItem()->getEntityId());
			echo Mage::getModel("catalog/category")->load($cat->getFirstItem()->getEntityId())->getUrlPath();
		} else {
			echo null; 
		}	
	}
}
