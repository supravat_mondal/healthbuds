<?php
$installer = $this;

$installer->startSetup();

$installer->getConnection()
    ->addColumn(
        $this->getTable('customer/customer_group'),
        'webform_id',
        'INT( 10 ) NOT NULL DEFAULT "0"'
    )
;

$installer->endSetup();