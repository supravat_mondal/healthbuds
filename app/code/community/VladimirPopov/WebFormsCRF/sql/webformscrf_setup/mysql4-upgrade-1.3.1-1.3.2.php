<?php
$installer = $this;

$installer->startSetup();

$installer->getConnection()
    ->addColumn(
        $this->getTable('customer/customer_group'),
        'crf_activation_status',
        'INT( 10 ) NOT NULL DEFAULT "1"'
    )
;


$installer->endSetup();