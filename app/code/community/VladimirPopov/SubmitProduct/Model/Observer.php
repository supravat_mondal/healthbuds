<?php
class VladimirPopov_SubmitProduct_Model_Observer{

    protected $result;
    protected $form;
	
	public function submitProduct($observer){
        
        // skip if the script is not enabled
		if(!Mage::getStoreConfig('webforms/submitproduct/enable')) return;
		
		$webform = $observer->getWebform();

        // list of allowed forms
        $allowed_forms = array('uploaddoctordata','uploadhspdata');

        // skip any non-allowed forms
        if(!in_array($webform->getCode(),$allowed_forms)) return;

        $this->form = $webform;
        $this->result = Mage::getModel('webforms/results')->load($observer->getResult()->getId());

        // list of categories ID numbers for new product
        $categories = array(12);

        // list of websites ID numbers for new product
        $websites = array(1);

        // tax class
        $tax_class = 0;

        // attribute set ID number
        $attribute_set = 11;

        // product type
        $type = 'virtual';

        // create product
        $product = Mage::getModel('catalog/product')->setStoreId(0);

        // set product type
        $product->setTypeId($type);

        // set attribute set
        $product->setAttributeSetId($attribute_set);

        // set categories
        $product->setCategoryIds($categories);

        // set websites
        $product->setWebsiteIDs($websites);

        // set tax class
        $product->setTaxClassId($tax_class);

        // generate unique SKU
        $product->setSku('doctor-data-'.Mage::helper('webforms')->randomAlphaNum());

        // set status disabled
        $product->setStatus(Mage_Catalog_Model_Product_Status::STATUS_DISABLED);

        // set visibility
        $product->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH);

        // set name
        $product->setName($this->getValueByCode('name'));

        // set description
        $product->setDescription($this->getValueByCode('description'));

        // set short description
        //$product->setShortDescription($this->getValueByCode('short_description'));

        // set weight
        //$product->setWeight($this->getValueByCode('weight'));

        // set price
        $product->setPrice($this->getValueByCode('price'));

        /* set stock
        $product->setStockData(array(
            'is_in_stock' => 1,
            'qty' => $this->getValueByCode('quantity')
        ));
*/

// data for doctor creation

		/// set chamber address
		$product->setAddress1($this->getValueByCode('address1'));
		// set Area
		//$product->setAreaKolkata($this->getValueByCode('area_kolkata'));
		// set City
		$product->setCity($this->getValueByCode('city-wb'));
		// set phone
		$product->setPhone($this->getValueByCode('phone'));
		// set email
		$product->setEmail($this->getValueByCode('email'));
		// set website
		//$product->setWebsite($this->getValueByCode('website'));
		
		// set visiting days 
		//$product->setAvailable_days($this->getValueByCode(array('available_days')));
		// set visiting time1
		$product->setVisit_time1($this->getValueByCode('visit_time1'));
		// set visiting time2
		$product->setVisit_time2($this->getValueByCode('visit_time2'));
		// set visiting time3
		$product->setVisit_time3($this->getValueByCode('visit_time3'));
		$product->setQualification($this->getValueByCode('qualification'));
		$product->setExperience($this->getValueByCode('experience'));
		$product->setNo_of_surgeries_conducted($this->getValueByCode('no_of_surgeries_conducted'));
		$product->setHome_visit($this->getValueByCode('home_visit'));
		$product->setHome_visit_charge($this->getValueByCode('home_visit_charge'));
		$product->setAvailability_in_emergency($this->getValueByCode('availability_in_emergency'));
		$product->setAvailable_skype_telemedicine($this->getValueByCode('available_skype_telemedicine'));
		$product->setCase_study($this->getValueByCode('case_study'));
		// set images
        if($this->getValueByCode('image1')){
            $image1 = $this->result->getFileFullPath($this->getFieldIdByCode('image1'), $this->getValueByCode('image1'));
			$product->addImageToMediaGallery($image1, array('image','small_image','thumbnail'));
        }
		/*set predefined images if 
		 if($this->getValueByCode('image3')){
            $image3 = $this->result->getFileFullPath($this->getFieldIdByCode('image3'), $this->getValueByCode('image3'));
			$product->addImageToMediaGallery($image3, array('image','small_image','thumbnail'));
        }
		*/
		// set images
		//$image1 = $this->result->getFileFullPath($this->getFieldIdByCode('image1'), $this->getValueByCode('image1'));
       // $product->addImageToMediaGallery($image1, array('image','small_image','thumbnail'));

        // set additional optional image
        if($this->getValueByCode('image2')){
            $image2 = $this->result->getFileFullPath($this->getFieldIdByCode('image2'), $this->getValueByCode('image2'));
            $product->addImageToMediaGallery($image2);
        }

        $product->save();
	}

    protected function getValueByCode($code){
        if(empty($this->result) || empty($this->form)) return false;

        foreach($this->form->getFieldsToFieldsets() as $fieldset){
            foreach($fieldset['fields'] as $field){
                if($field->getCode() == $code){
                    return $this->result->getData('field_'.$field->getId());
                }
            }
        }

    }

    protected function getFieldIdByCode($code){
        if(empty($this->result) || empty($this->form)) return false;

        foreach($this->form->getFieldsToFieldsets() as $fieldset){
            foreach($fieldset['fields'] as $field){
                if($field->getCode() == $code){
                    return $field->getId();
                }
            }
        }

    }
	
}
