<?php 
/**
 * MagPassion_Ajaxbookmarks extension
 * 
 * @category   	MagPassion
 * @package		MagPassion_Ajaxbookmarks
 * @copyright  	Copyright (c) 2014 by MagPassion (http://magpassion.com)
 * @license	http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
/**
 * AjaxBookmark front contrller
 *
 * @category	MagPassion
 * @package		MagPassion_Ajaxbookmarks
 * @author MagPassion.com
 */
class MagPassion_Ajaxbookmarks_AjaxbookmarkController extends Mage_Core_Controller_Front_Action{
	/**
 	 * default action
 	 * @access public
 	 * @return void
 	 * @author MagPassion.com
 	 */
 	public function indexAction(){
		$this->loadLayout();
 		
		$this->renderLayout();
	}
}