<?php
/**
 * MagPassion_Ajaxbookmarks extension
 * 
 * @category   	MagPassion
 * @package		MagPassion_Ajaxbookmarks
 * @copyright  	Copyright (c) 2014 by MagPassion (http://magpassion.com)
 * @license	http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
/**
 * More Extension admin controller
 *
 * @category	MagPassion
 * @package		MagPassion_Ajaxbookmarks
 * @author MagPassion.com
 */
class MagPassion_Ajaxbookmarks_Adminhtml_Ajaxbookmarks_MoreextensionController extends MagPassion_Ajaxbookmarks_Controller_Adminhtml_Ajaxbookmarks{
	/**
	 * init the moreextension
	 * @access protected
	 * @return MagPassion_Ajaxbookmarks_Model_Moreextension
	 */
	protected function _initMoreextension(){
		
	}
 	/**
	 * default action
	 * @access public
	 * @return void
	 * @author MagPassion.com
	 */
	public function indexAction() {
		$this->loadLayout();
		$this->_title(Mage::helper('ajaxbookmarks')->__('Ajaxbookmarks'))
			 ->_title(Mage::helper('ajaxbookmarks')->__('More Extensions'));
        
		$this->renderLayout();
     
	}
}