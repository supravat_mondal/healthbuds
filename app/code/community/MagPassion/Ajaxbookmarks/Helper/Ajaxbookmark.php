<?php 
/**
 * MagPassion_Ajaxbookmarks extension
 * 
 * @category   	MagPassion
 * @package		MagPassion_Ajaxbookmarks
 * @copyright  	Copyright (c) 2014 by MagPassion (http://magpassion.com)
 * @license	http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
/**
 * AjaxBookmark helper
 *
 * @category	MagPassion
 * @package		MagPassion_Ajaxbookmarks
 * @author MagPassion.com
 */
class MagPassion_Ajaxbookmarks_Helper_Ajaxbookmark extends Mage_Core_Helper_Abstract{
	/**
	 * @access public
	 * @return bool
	 * @author MagPassion.com
	 */
    
    public function getshowtitletext(){
		return Mage::getStoreConfig('ajaxbookmarks/toplinkoption/showtitletext', Mage::app()->getStore());;
	}
    
    public function gettitle(){
        $title = Mage::getStoreConfig('ajaxbookmarks/toplinkoption/title', Mage::app()->getStore());
        if (!$title) $title = 'Bookmark';
		return $title;
	}
    public function getstarskin(){
		return Mage::getStoreConfig('ajaxbookmarks/toplinkoption/starskin', Mage::app()->getStore());
	}
    
    public function getposition(){
		return Mage::getStoreConfig('ajaxbookmarks/toplinkoption/position', Mage::app()->getStore());;
	}
    
    public function getblocktitle(){
		return Mage::getStoreConfig('ajaxbookmarks/bookmarklistoption/blocktitle', Mage::app()->getStore());;
	}
    
    public function getblockwidth(){
		return Mage::getStoreConfig('ajaxbookmarks/bookmarklistoption/blockwidth', Mage::app()->getStore());;
	}
    
    public function getblockheight(){
		return Mage::getStoreConfig('ajaxbookmarks/bookmarklistoption/blockheight', Mage::app()->getStore());;
	}
    
    public function getproductwidth(){
		return Mage::getStoreConfig('ajaxbookmarks/bookmarklistoption/productwidth', Mage::app()->getStore());;
	}
    
    public function getproductheight(){
		return Mage::getStoreConfig('ajaxbookmarks/bookmarklistoption/productheight', Mage::app()->getStore());
	}
    
    public function getshowimage(){
		return Mage::getStoreConfig('ajaxbookmarks/bookmarklistoption/showimage', Mage::app()->getStore());;
	}
    
    public function getimagetwidth(){
		return Mage::getStoreConfig('ajaxbookmarks/bookmarklistoption/imagewidth', Mage::app()->getStore());
	}
    
    public function getimageheight(){
		return Mage::getStoreConfig('ajaxbookmarks/bookmarklistoption/imageheight', Mage::app()->getStore());
	}
    
    public function getshowname(){
		return Mage::getStoreConfig('ajaxbookmarks/bookmarklistoption/showname', Mage::app()->getStore());
	}
    
    public function getshowreview(){
		return Mage::getStoreConfig('ajaxbookmarks/bookmarklistoption/showreview', Mage::app()->getStore());
	}
    
    public function getshowprice(){
		return Mage::getStoreConfig('ajaxbookmarks/bookmarklistoption/showprice', Mage::app()->getStore());
	}
    
    public function getshowaddtocart(){
		return Mage::getStoreConfig('ajaxbookmarks/bookmarklistoption/showaddtocart', Mage::app()->getStore());
	}
    
    public function getshowaddtolink(){
		return Mage::getStoreConfig('ajaxbookmarks/bookmarklistoption/showaddtolink', Mage::app()->getStore());
	}
    
    public function loadjquery(){
		return Mage::getStoreConfig('ajaxbookmarks/jquery/loadjquery', Mage::app()->getStore());
	}
}