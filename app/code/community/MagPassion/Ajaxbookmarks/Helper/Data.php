<?php 
/**
 * MagPassion_Ajaxbookmarks extension
 * 
 * @category   	MagPassion
 * @package		MagPassion_Ajaxbookmarks
 * @copyright  	Copyright (c) 2014 by MagPassion (http://magpassion.com)
 * @license	http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
/**
 * Ajaxbookmarks default helper
 *
 * @category	MagPassion
 * @package		MagPassion_Ajaxbookmarks
 * @author MagPassion.com
 */
class MagPassion_Ajaxbookmarks_Helper_Data extends Mage_Core_Helper_Abstract{
	/**
	 * get the url to the ajaxbookmarks list page
	 * @access public
	 * @return string
	 * @author MagPassion.com
	 */
	public function getAjaxbookmarksUrl(){
		return Mage::getUrl('ajaxbookmarks/ajaxbookmark/index');
	}
}