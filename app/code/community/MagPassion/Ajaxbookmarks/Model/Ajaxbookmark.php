<?php
/**
 * MagPassion_Ajaxbookmarks extension
 * 
 * @category   	MagPassion
 * @package		MagPassion_Ajaxbookmarks
 * @copyright  	Copyright (c) 2014 by MagPassion (http://magpassion.com)
 * @license	http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
/**
 * AjaxBookmark model
 *
 * @category	MagPassion
 * @package		MagPassion_Ajaxbookmarks
 * @author MagPassion.com
 */
class MagPassion_Ajaxbookmarks_Model_Ajaxbookmark extends Mage_Core_Model_Abstract{
	/**
	 * Entity code.
	 * Can be used as part of method name for entity processing
	 */
	const ENTITY= 'ajaxbookmarks_ajaxbookmark';
	const CACHE_TAG = 'ajaxbookmarks_ajaxbookmark';
	/**
	 * Prefix of model events names
	 * @var string
	 */
	protected $_eventPrefix = 'ajaxbookmarks_ajaxbookmark';
	
	/**
	 * Parameter name in event
	 * @var string
	 */
	protected $_eventObject = 'ajaxbookmark';
	/**
	 * constructor
	 * @access public
	 * @return void
	 * @author MagPassion.com
	 */
	public function _construct(){
		parent::_construct();
		$this->_init('ajaxbookmarks/ajaxbookmark');
	}
	/**
	 * before save ajaxbookmark
	 * @access protected
	 * @return MagPassion_Ajaxbookmarks_Model_Ajaxbookmark
	 * @author MagPassion.com
	 */
	protected function _beforeSave(){
		parent::_beforeSave();
		$now = Mage::getSingleton('core/date')->gmtDate();
		if ($this->isObjectNew()){
			$this->setCreatedAt($now);
		}
		$this->setUpdatedAt($now);
		return $this;
	}
	/**
	 * get the url to the ajaxbookmark details page
	 * @access public
	 * @return string
	 * @author MagPassion.com
	 */
	public function getAjaxbookmarkUrl(){
		return Mage::getUrl('ajaxbookmarks/ajaxbookmark/view', array('id'=>$this->getId()));
	}
	/**
	 * save ajaxbookmark relation
	 * @access public
	 * @return MagPassion_Ajaxbookmarks_Model_Ajaxbookmark
	 * @author MagPassion.com
	 */
	protected function _afterSave() {
		return parent::_afterSave();
	}
}