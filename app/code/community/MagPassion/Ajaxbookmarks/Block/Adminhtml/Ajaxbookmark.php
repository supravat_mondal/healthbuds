<?php
/**
 * MagPassion_Ajaxbookmarks extension
 * 
 * @category   	MagPassion
 * @package		MagPassion_Ajaxbookmarks
 * @copyright  	Copyright (c) 2014 by MagPassion (http://magpassion.com)
 * @license	http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
/**
 * AjaxBookmark admin block
 *
 * @category	MagPassion
 * @package		MagPassion_Ajaxbookmarks
 * @author MagPassion.com
 */
class MagPassion_Ajaxbookmarks_Block_Adminhtml_Ajaxbookmark extends Mage_Adminhtml_Block_Widget_Grid_Container{
	/**
	 * constructor
	 * @access public
	 * @return void
	 * @author MagPassion.com
	 */
	public function __construct(){
		$this->_controller 		= 'adminhtml_ajaxbookmark';
		$this->_blockGroup 		= 'ajaxbookmarks';
		$this->_headerText 		= Mage::helper('ajaxbookmarks')->__('AjaxBookmark');
		$this->_addButtonLabel 	= Mage::helper('ajaxbookmarks')->__('Add AjaxBookmark');
		parent::__construct();
	}
}