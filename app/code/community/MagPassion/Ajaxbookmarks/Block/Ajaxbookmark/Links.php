<?php
/**
 * MagPassion_Ajaxbookmarks extension
 * 
 * @category   	MagPassion
 * @package		MagPassion_Ajaxbookmarks
 * @copyright  	Copyright (c) 2014 by MagPassion (http://magpassion.com)
 * @license	http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Links block
 *
 * @category   	MagPassion
 * @package		MagPassion_Ajaxbookmarks
 * @author      MagPassion.com
 */
class MagPassion_Ajaxbookmarks_Block_Ajaxbookmark_Links extends Mage_Core_Block_Template
{
    public function addBookmarkLink()
    {
        $parentBlock = $this->getParentBlock();
        
        $titlestyle = Mage::helper('ajaxbookmarks/ajaxbookmark')->getshowtitletext();
        $title = Mage::helper('ajaxbookmarks/ajaxbookmark')->gettitle();
        $skin = Mage::helper('ajaxbookmarks/ajaxbookmark')->getstarskin();
        $position = Mage::helper('ajaxbookmarks/ajaxbookmark')->getposition();
        $text = '<span id="ajaxbm-top-link" class="ajaxbm-top-link"';
        if ($titlestyle == 1) $text .= ' style="margin-left: 10px;"';
        $text .= '>';
        if ($titlestyle == 0) $text .= '<span class="titlewithstar">'.$title.'&nbsp;</span><span id="ajaxbm-star" class="star '.$skin.'"></span>';
        elseif ($titlestyle == 1) $text .= '<span id="ajaxbm-star" class="star '.$skin.'"></span>';
        else $text .= '<span class="title">'.$title.'</span>';
        $text .= '<label id="count_bm" style="display: none">1</label></span>';
        $parentBlock->addLink($text, '#', 'Ajax bookmark', true, array(), $position, null, 'class="ajaxbookmark" id="ajaxbookmark"');
        
        return $this;
    }
}
