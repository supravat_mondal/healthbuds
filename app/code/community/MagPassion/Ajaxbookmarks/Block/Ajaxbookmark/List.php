<?php 
/**
 * MagPassion_Ajaxbookmarks extension
 * 
 * @category   	MagPassion
 * @package		MagPassion_Ajaxbookmarks
 * @copyright  	Copyright (c) 2014 by MagPassion (http://magpassion.com)
 * @license	http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
/**
 * AjaxBookmark list block
 *
 * @category	MagPassion
 * @package		MagPassion_Ajaxbookmarks
 * @author MagPassion.com
 */
class MagPassion_Ajaxbookmarks_Block_Ajaxbookmark_List extends Mage_Core_Block_Template{
	/**
	 * initialize
	 * @access public
	 * @return void
	 * @author MagPassion.com
	 */
 	public function __construct(){
		parent::__construct();
	}
	/**
	 * prepare the layout
	 * @access protected
	 * @return MagPassion_Ajaxbookmarks_Block_Ajaxbookmark_List
	 * @author MagPassion.com
	 */
	protected function _prepareLayout(){
		parent::_prepareLayout();
		//if ($head = $this->getLayout()->getBlock('head')) {
         //   $head->setTitle(''); 
            
        //}
	}
	/**
	 * get the pager html
	 * @access public
	 * @return string
	 * @author MagPassion.com
	 */
	public function getPagerHtml(){
		return $this->getChildHtml('pager');
	}
}
