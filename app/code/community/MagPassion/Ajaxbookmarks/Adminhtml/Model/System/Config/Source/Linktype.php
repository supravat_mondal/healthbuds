<?php

/**
 * MagPassion_Ajaxbookmarks extension
 * 
 * @category   	MagPassion
 * @package		MagPassion_Ajaxbookmarks
 * @copyright  	Copyright (c) 2014 by MagPassion (http://magpassion.com)
 * @license	http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class MagPassion_Ajaxbookmarks_Adminhtml_Model_System_Config_Source_Linktype
{

    /**
     * Link type
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => 0, 'label'=>Mage::helper('adminhtml')->__('Title and star')),
            array('value' => 1, 'label'=>Mage::helper('adminhtml')->__('Star only')),
            array('value' => 2, 'label'=>Mage::helper('adminhtml')->__('Title only')),
        );
    }

}

?>
