<?php

/**
 * MagPassion_Ajaxbookmarks extension
 * 
 * @category   	MagPassion
 * @package		MagPassion_Ajaxbookmarks
 * @copyright  	Copyright (c) 2014 by MagPassion (http://magpassion.com)
 * @license	http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class MagPassion_Ajaxbookmarks_Adminhtml_Model_System_Config_Source_Starskin
{

    /**
     * Star skin
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => 'white', 'label'=>Mage::helper('adminhtml')->__('White')),
            array('value' => 'black', 'label'=>Mage::helper('adminhtml')->__('Black')),
            array('value' => 'yellow', 'label'=>Mage::helper('adminhtml')->__('Yellow')),
            array('value' => 'cyan', 'label'=>Mage::helper('adminhtml')->__('Cyan')),
            array('value' => 'blue', 'label'=>Mage::helper('adminhtml')->__('Blue')),
        );
    }

}

?>
