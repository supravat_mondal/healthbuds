<?php
/**
 * @category    Fishpig
 * @package    Fishpig_SmartTabs
 * @license      http://fishpig.co.uk/license.txt
 * @author       Ben Tideswell <ben@fishpig.co.uk>
 */

class Fishpig_SmartTabs_Block_Tabs extends Mage_Catalog_Block_Product_View_Tabs
{
	/**
	 * Determine whether this is enabled for the product page
	 *
	 * @return bool
	 */
	public function isEnabled()
	{
		return Mage::getStoreConfigFlag('smarttabs/product/enabled');
	}

	/**
	 * Determine whether this is enabled for the product page
	 *
	 * @return bool
	 */
	public function getTabIntegrationMethod()
	{
		return trim(Mage::getStoreConfig('smarttabs/product/integration_method'));
	}

	/**
	 * Determine whether to use the detailed_info integration method
	 *
	 * @return bool
	 */
	public function isIntegratedUsingChildGroup()
	{
		return $this->getTabIntegrationMethod() === Fishpig_SmartTabs_Model_System_Config_Source_Integration_Method::METHOD_DETAILED_INFO || Mage::getSingleton('core/design_package')->getPackageName() === 'rwd';
	}
	
	/**
	 * Determine whether to use the info_tabs integration method
	 *
	 * @return bool
	 */
	public function isDefaultIntegrationMethod()
	{
		return !$this->getTabIntegrationMethod() 
			|| $this->getTabIntegrationMethod() === Fishpig_SmartTabs_Model_System_Config_Source_Integration_Method::METHOD_DEFAULT;
	}

	/**
	 * Retrieve the current product model
	 *
	 * @return Mage_Catalog_Model_Product
	 */	
	public function getProduct()
	{
		return Mage::registry('product');
	}

	/**
	 * Add a SmartTab to the list
	 *
	 * @param string $alias
	 * @param string $title
	 * @param Mage_Core_Block_Abstract $block
	 * @return false|$this
	 */
	function addSmartTab($alias, $title, $block)
	{
		if (!$this->isEnabled() || !$title || !$block) {
			return false;
		}
	
		$existingIndex = false;
		
		foreach($this->_tabs as $index => $tab) {
			if ($tab['alias'] === $alias) {
				$existingIndex = $index;
				break;
			}
		}

		if ($existingIndex !== false) {
			$this->_tabs[$existingIndex] = array(
				'alias' => $alias,
				'title' => $title
			);
		}
		else {
			$this->_tabs[] = array(
				'alias' => $alias,
				'title' => $title
			);
		}
	
		$this->setChild($alias, $block);
		
		return $this;
	}	

	/**
	 * Add blocks to detailed_info
	 *
	 * @return $this
	 */
	protected function _prepareLayout()
	{
		if ($this->isEnabled()) {
			if ($this->isIntegratedUsingChildGroup()) {
				if ($tabs = $this->getSmartTabs()) {
					$productInfo = $this->getLayout()->getBlock('product.info');
	
					foreach($tabs as $tab) {
						if ($tab->canDisplay() && ($tabBlock = $tab->getBlock()) !== false) {
							$productInfo->insert($tabBlock)->addToChildGroup('detailed_info', $tabBlock);
						}
					}
				}
			}
		}
		
		return parent::_prepareLayout();
	}

	
	/**
	 * Prepare the tabs before displaying
	 *
	 * @return $this
	 */
	protected function _beforeToHtml()
	{
		if ($this->isEnabled()) {
			if ($tabs = $this->getSmartTabs()) {
				foreach($tabs as $tab) {
					if ($tab->canDisplay()) {
						if (($tabBlock = $tab->getBlock()) !== false) {
							$this->addSmartTab($tab->getAlias(), $tab->getTitle(), $tabBlock);
						}
					}
				}
			}
		}

		return parent::_beforeToHtml();
	}
	
	/**
	 * Retrieve the tabs collection
	 *
	 * @return Fishpig_SmartTabs_Model_Resource_Tab_Collection
	 */
	public function getSmartTabs()
	{
		$tabs = Mage::getResourceModel('smarttabs/tab_collection')
			->addStoreFilter(Mage::app()->getStore())
			->addFieldToFilter('status', 1)
			->load()
			->walk('afterLoad');
		
		$tabsArray = array();
		
		foreach($tabs as $key => $tab) {
			if (!$tab->canApplyToProduct($this->getProduct())) {
				unset($tabs[$key]);
			}
		}
		
		$smartTabs = array();
		
		foreach($tabs as $key => $tab) {
			$alias = $tab->getAlias();
			
			if (!isset($smartTabs[$alias])) {
				$smartTabs[$alias] = $tab;
			}
			else if (!$tab->getFilters()) {
				continue;
			}
			else if (!$smartTabs[$alias]->getFilters()) {
				$smartTabs[$alias] = $tab;
			}
			else {
			print_r($smartTabs[$alias]->getFilters());exit;
			}
		}
		
		return $smartTabs;
	}	
	
	/**
	 * Get the JS file URL
	 *
	 * @return string
	 */
	public function getTabsJsUrl()
	{
		return Mage::getBaseUrl('js') . 'fishpig/smarttabs/tabs.js';
	}
}
