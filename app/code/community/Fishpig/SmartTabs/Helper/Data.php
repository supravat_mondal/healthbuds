<?php
/**
 * @category    Fishpig
 * @package    Fishpig_SmartTabs
 * @license      http://fishpig.co.uk/license.txt
 * @author       Ben Tideswell <ben@fishpig.co.uk>
 */

class Fishpig_SmartTabs_Helper_Data extends Mage_Core_Helper_Abstract
{
	public function getFullTabsHtml()
	{
		return Mage::getSingleton('core/layout')
			->createBlock('smarttabs/tabs')
			->setTemplate('smarttabs/tabs.phtml')
			->toHtml();
	}
}
