<?php
/**
 * Intellimage
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to mauricioprado00@gmail.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade your
 * Intellimage extension to newer versions in the future. 
 * If you wish to customize your Intellimage extension to your
 * needs please refer to mauricioprado00@gmail.com for more information.
 *
 * @package     Intellimage_CustomRelations
 * @author      Hugo Mauricio Prado Macat
 * @copyright   2013
 * @email       mauricioprado00@gmail.com
 * @license     http://www.opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


$installer = $this;

$installer->startSetup();

$connection = $installer->getConnection();
$table = $installer->getTable('catalog/eav_attribute');
$tableDescription = $connection->describeTable($table);
if (!isset($tableDescription['is_product_link_type'])) {
    $installer->getConnection()
        ->addColumn(
            $installer->getTable('catalog/eav_attribute'),
            'is_product_link_type', 
            "smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Product Link Type'"
        );
}


$data = array (
  'attribute_code' => 'product_accesories',
  'is_global' => '0',
  'frontend_input' => 'multiselect',
  'default_value_text' => '',
  'default_value_yesno' => '0',
  'default_value_date' => '',
  'default_value_textarea' => '',
  'is_product_link_type' => '1',
  'is_unique' => '0',
  'is_required' => '0',
  'is_configurable' => '0',
  'is_searchable' => '0',
  'is_visible_in_advanced_search' => '0',
  'is_comparable' => '0',
  'is_filterable' => '0',
  'is_filterable_in_search' => '0',
  'is_used_for_promo_rules' => '0',
  'is_html_allowed_on_front' => '1',
  'is_visible_on_front' => '0',
  'used_in_product_listing' => '0',
  'frontend_label' => 
  array (
    0 => 'Accesories',
  ),
  'option' => 
  array (
    'value' => 
    array (
      'option_8' => 
      array (
        0 => 'On the left',
      ),
      'option_7' => 
      array (
        0 => 'Large template',
      ),
      'option_6' => 
      array (
        0 => 'Accesories Title',
      ),
      'option_5' => 
      array (
        0 => 'Before all',
      ),
      'option_4' => 
      array (
        0 => 'Dont show',
      ),
      'option_3' => 
      array (
        0 => 'On the content',
      ),
      'option_2' => 
      array (
        0 => 'On the right',
      ),
      'option_1' => 
      array (
        0 => 'On product view',
      ),
      'option_0' => 
      array (
        0 => 'After all',
      ),
    ),
    'order' => 
    array (
      'option_8' => '3',
      'option_7' => '8',
      'option_6' => '7',
      'option_5' => '6',
      'option_4' => '0',
      'option_3' => '1',
      'option_2' => '2',
      'option_1' => '4',
      'option_0' => '5',
    ),
    'delete' => 
    array (
      'option_7' => '',
      'option_6' => '',
      'option_5' => '',
      'option_4' => '',
      'option_3' => '',
      'option_2' => '',
      'option_1' => '',
      'option_0' => '',
    ),
  ),
);


if (count(Mage::app()->getStores()) === 0) {
    Mage::app()->reinitStores();
}

foreach (Mage::getModel('core/store')->getCollection() as $store) {
    $id = $store->getId();
    $data['frontend_label'][$id] = 'Accesories';
    $data['option']['value']['option_8'][$id] = 'position:left';
    $data['option']['value']['option_7'][$id] = 'template:catalog/product/list/any2.phtml';
    $data['option']['value']['option_6'][$id] = 'title:Accesories';
    $data['option']['value']['option_5'][$id] = 'before:-';
    $data['option']['value']['option_4'][$id] = '0';
    $data['option']['value']['option_3'][$id] = 'position:content';
    $data['option']['value']['option_2'][$id] = 'position:right';
    $data['option']['value']['option_1'][$id] = 'position:product.info.additional';
    $data['option']['value']['option_0'][$id] = 'after:-';
}

//create the attribute
/* @var $attribute Mage_Catalog_Model_Entity_Attribute */
$attribute = Mage::getModel('catalog/resource_eav_attribute');
/* @var $helper Mage_Catalog_Helper_Product */
$helper = Mage::helper('catalog/product');
$data['source_model'] = $helper->getAttributeSourceModelByInputType($data['frontend_input']);
$data['backend_model'] = $helper->getAttributeBackendModelByInputType($data['frontend_input']);
$data['is_configurable'] = 0;
$data['is_filterable'] = 0;
$data['is_filterable_in_search'] = 0;
$data['backend_type'] = $attribute->getBackendTypeByInput($data['frontend_input']);
$defaultValueField = $attribute->getDefaultValueByInput($data['frontend_input']);
if ($defaultValueField) {
    $data['default_value'] = $this->getRequest()->getParam($defaultValueField);
}
$attribute->addData($data);
$entityTypeProduct = Mage::getModel('eav/entity')->setType(Mage_Catalog_Model_Product::ENTITY)->getTypeId();
$attribute->setEntityTypeId($entityTypeProduct);
$attribute->setIsUserDefined(1);
$attribute->save();

Mage::helper('customRelations/link')->createForAttributeIfNeeded($attribute);

//add attribute to "default" set and group with sort_order = 1
$attributeId = $attribute->getAttributeId();
$set = Mage::getModel('eav/entity_attribute_set')->getCollection()
    ->addFieldToFilter('attribute_set_name', 'Default')
    ->addFieldToFilter('entity_type_id', $entityTypeProduct)
    ->getFirstItem();
if ($set) {
    $attributeSetId = $set->getData('attribute_set_id');
    $group = Mage::getModel('eav/entity_attribute_group')->getCollection()
        ->addFieldToFilter('attribute_set_id', $attributeSetId)
        ->addFieldToFilter('sort_order', '1')
        ->getFirstItem();
    if ($group) {
        $attributeGroupId = $group->getAttributeGroupId();
        $entityAttribute = Mage::getModel('eav/entity_attribute')
            ->setEntityTypeId($entityTypeProduct)
            ->setAttributeSetId($attributeSetId)
            ->setAttributeGroupId($attributeGroupId)
            ->setAttributeId($attributeId);
        $entityAttribute->save();
    }
}


$installer->endSetup(); 
