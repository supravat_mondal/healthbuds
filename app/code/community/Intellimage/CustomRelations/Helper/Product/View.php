<?php
/**
 * Intellimage
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to mauricioprado00@gmail.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade your
 * Intellimage extension to newer versions in the future. 
 * If you wish to customize your Intellimage extension to your
 * needs please refer to mauricioprado00@gmail.com for more information.
 *
 * @package     Intellimage_CustomRelations
 * @author      Hugo Mauricio Prado Macat
 * @copyright   2013
 * @email       mauricioprado00@gmail.com
 * @license     http://www.opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */



class Intellimage_CustomRelations_Helper_Product_View extends Mage_Core_Helper_Abstract
{
    const DEFAULT_POSITION = 'right';
    const DEFAULT_TYPE = 'customRelations/catalog_product_list';
    
    private static $_blacklist_attributes = array('position');
    
    private static $_xmlEntities = array(
        '<' => '&lt;',
        '>' => '&gt;',
        '&' => '&amp;',
        '"' => '&quot;',
        "'" => '&apos;'
    );
    
    /**
     * Iterates through product attributes and 
     * adds a block for each product link type attribute
     * @param Mage_Catalog_Model_Product $product
     */
    public function addCustomRelationsBlocks($product)
    {
        $attributes = $product->getAttributes();
        foreach ($attributes as $attribute) {
            if ($attribute->getIsProductLinkType()) {
                /** @var $linkType Intellimage_CustomRelations_Model_Product_Link_Type */
                $linkType = Mage::helper('customRelations/link')->existsForAttribute($attribute);
                $this->addCustomRelationsBlock($product, $attribute, $linkType);
            }
        }
    }
    
    /**
     * Add a product link type list for a given product attribute
     * @param Mage_Catalog_Model_Product $product
     * @param Mage_Catalog_Model_Resource_Eav_Attribute $attribute
     * @param Intellimage_CustomRelations_Model_Product_Link_Type $linkType
     * @return type
     */
    public function addCustomRelationsBlock($product, $attribute, $linkType)
    {
        $attributeCode = $attribute->getAttributeCode();
        $value = $product->getData($attributeCode);

        //positions where the block should be added (if none, then it wont be added)
        $positions = array();
        $blockAttributes = array();
        $blockAttributes['title'] = $attribute->getStoreLabel();
        switch ($attribute->getFrontendInput()) {
            case "multiselect":
                $value = explode(',', $value);
                $fa = $attribute->getFrontend();
                foreach ($fa->getSelectOptions() as $selopt) {
                    if (in_array($selopt['value'], $value)) {
                        $this->_addPosition($selopt['label'], $positions);
                        $this->_addBlockAttributes($selopt['label'], $blockAttributes);
                    }
                };
                break;
            default:
                $this->_addPosition($value, $positions, $this->getDefaultPosition());
                $this->_addBlockAttributes($value, $blockAttributes);
                break;
        }
        if (count($positions)) {
            foreach ($positions as $position) {
                $this->addCustomRelationBlock($product, $attribute, $linkType, $value, $position, $blockAttributes);
            }
        }
        return;
    }
    
    /**
     * Adds a attributes for the block to the given array
     * @param string $value
     * @param array $blockAttributes
     * @return boolean
     */
    private function _addBlockAttributes($value, &$blockAttributes)
    {
        if (strpos($value, ';') !== false) {
            $values = explode(';', $value);
            foreach ($values as $value) {
                $this->_addBlockAttributes($value, $blockAttributes);
            }
        } else {
            $c = preg_match(
                '(\\b(?P<attribute_name>[a-zA-Z]+):\\s*(?P<attribute_value>.+?)\\s*$)', 
                $value, $matches
            );
            if ($c) {
                $attribute_name = $matches['attribute_name'];
                if (!in_array($attribute_name, self::$_blacklist_attributes)) {
                    $attribute_value = $matches['attribute_value'];
                    if (isset($blockAttributes[$attribute_name])) {
                        if (!is_array($blockAttributes[$attribute_name])) {
                            $blockAttributes[$attribute_name] = array($blockAttributes[$attribute_name]);
                        }
                        $blockAttributes[$attribute_name][] = $attribute_value;
                    } else {
                        $blockAttributes[$attribute_name] = $attribute_value;
                    }
                }
            }
        }
    }
    
    /**
     * Adds a position to the given array
     * @param string $value
     * @param array $positions
     * @param string $defaultPosition
     * @return boolean
     */
    private function _addPosition($value, &$positions, $defaultPosition = null)
    {
        $position = $this->getPositionFromValue($value, $defaultPosition);
        if (isset($position)) {
            $positions[] = $position;
            return true;
        }
        return false;
    }
    
    /**
     * 
     * @param Mage_Catalog_Model_Product $product
     * @param Mage_Catalog_Model_Resource_Eav_Attribute $attribute
     * @param Intellimage_CustomRelations_Model_Product_Link_Type $linkType
     * @param mixed $value
     * @param string $position name of the block
     * @param array $blockAttributes
     * @return type
     */
    public function addCustomRelationBlock($product, 
            $attribute, $linkType, $value, $position, $blockAttributes = array())
    {
        $layoutElementClassname = Mage::getConfig()->getGroupedClassName("model", 'core/layout_element');
        $config = $this->getBlockConfig($product, $attribute, $value, $position);
        $name = isset($config['name']) ? $config['name'] : 
            'catalog.product.custonrelations' . $attribute->getAttributeCode();
        
        $attributes = "";
        foreach ($blockAttributes as $attributeName => $attributeValue) {
            $attributeValue = strtr($attributeValue, self::$_xmlEntities);
            $attributes .= " $attributeName = \"$attributeValue\"";
        }
        
        $name .= ".$position";
        
        $xml = <<< XML
        <layout>
            <reference name="$position">
                <block type="{$config['type']}" name="$name" $attributes>
                    <action method="setProductLinkTypeCode">
                        <code>{$linkType->getCode()}</code>
                    </action>
                </block>
            </reference>
        </layout>
XML;
//                var_dump($config);
//                echo " {$attribute->getAttributeCode()}\n$xml\n\n";
        $element = simplexml_load_string($xml, $layoutElementClassname);
        $layout = Mage::app()->getLayout();
        $layout->generateBlocks($element);
        $block = $layout->getBlock($name);
        $attrs = array_merge($config, $blockAttributes);
        $block->addData($attrs);
    }
    
    /**
     * 
     * @param Mage_Catalog_Model_Product $product
     * @param Mage_Catalog_Model_Resource_Eav_Attribute $attribute
     * @param mixed $value
     * @param string $position name of the block
     * @return type
     */
    public function getBlockConfig($product, $attribute, $value, $position)
    {
        return array(
            'type' => self::DEFAULT_TYPE,
            'template' => Mage::helper('customRelations')->getDefaultTemplate()
        );
    }

    /**
     * Extracts a position from a string value. the format is "position:[A-Z_0-9]+", or just ^[A-Z_0-9]+$
     * @param string $value
     * @param string $default
     * @return type
     */
    public function getPositionFromValue($value, $default = null) 
    {
        if (preg_match('(\\bposition:\\s*(?P<position>[a-zA-Z0-9_.]+))', $value, $matches)) {
            return $matches['position'];
        } else if (preg_match('(^[A-Za-z]+[A-Za-z_0-9]+$)', $value) && $value !== 'no') {
            return $value;
        } else if ($value && isset($default)) {
            return $default;
        }

        return null;
    }
    
    /**
     * returns the default block name to which the list blocks should be attached
     * @return string
     */
    public function getDefaultPosition()
    {
        return self::DEFAULT_POSITION;
    }
}