<?php
/**
 * Intellimage
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to mauricioprado00@gmail.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade your
 * Intellimage extension to newer versions in the future. 
 * If you wish to customize your Intellimage extension to your
 * needs please refer to mauricioprado00@gmail.com for more information.
 *
 * @package     Intellimage_CustomRelations
 * @author      Hugo Mauricio Prado Macat
 * @copyright   2013
 * @email       mauricioprado00@gmail.com
 * @license     http://www.opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */



class Intellimage_CustomRelations_Helper_Link extends Mage_Core_Helper_Abstract
{
    const CUSTOM_CODE_PREFIX = 'custom_';
    
    /**
     * Creates a product link for a specific type
     * @param string $productLinkTypeCode
     * @return Mage_Catalog_Model_Product_Link
     */
    public function createProductLink($productLinkTypeCode)
    {
        $link = Mage::getModel('catalog/product_link');
        $linkTypeId = $this->getProductLinkTypeIdByCode($productLinkTypeCode);
        if ($linkTypeId) {
            $link->setLinkTypeId($linkTypeId);
            return $link;
        }
        return null;
    }
    
    /**
     * Returns the instance that matches the given code.
     * @param string $productLinkTypeCode
     * @return Intellimage_CustomRelations_Model_Product_Link_Type
     */
    public function getProductLinkTypeByCode($productLinkTypeCode)
    {
        $collection = Mage::getModel('customRelations/product_link_type')->getCollection();
        $collection->addFieldToFilter('code', $productLinkTypeCode);
        if ($collection->count()) {
            return $collection->getFirstItem();
        }
        return null;
    }
    
    /**
     * returns the product link type id that matches the given code
     * @param type $productLinkTypeCode
     * @return int
     */
    public function getProductLinkTypeIdByCode($productLinkTypeCode)
    {
        $linkType = $this->getProductLinkTypeByCode($productLinkTypeCode);
        if ($linkType) {
            return $linkType->getLinkTypeId();
        }
        return null;
    }
    
    /**
     * Creates a product link for an attribute when its needed
     * @param Mage_Catalog_Model_Resource_Eav_Attribute $attribute
     * @return Intellimage_CustomRelations_Model_Product_Link_Type
     */
    public function createForAttributeIfNeeded($attribute)
    {
        if ($this->isProductLinkTypeAttribute($attribute)) {
            if (!$this->existsForAttribute($attribute)) {
                $this->createForAttribute($attribute);
            }
        }
        return $this;
    }
    
    /**
     * determines if the attribute is a product link type attribute
     * @param Mage_Catalog_Model_Resource_Eav_Attribute $attribute
     * @return boolean
     */
    public function isProductLinkTypeAttribute($attribute)
    {
        if ($attribute->getIsProductLinkType()) {
            return true;
        }
        return false;
    }
    
    /**
     * Determines if the product link type exists for the given attribute.
     * If it does, the instance of the link is returned, if it doesnt
     * null is returned.
     * @param Mage_Catalog_Model_Resource_Eav_Attribute $attribute
     * @return Intellimage_CustomRelations_Model_Product_Link_Type
     */
    public function existsForAttribute($attribute)
    {
       $collection = Mage::getModel('customRelations/product_link_type')->getCollection();
       $collection->addFieldToFilter("code", $this->_getLinkCodeFromAttributeCode($attribute->getAttributeCode()));
       if ($collection->count()) {
           return $collection->getFirstItem();
       }
       return null;
    }
    
    /**
     * returns the attribute associated with the link type
     * @param string $linkTypeCode
     * @return Mage_Catalog_Model_Resource_Eav_Attribute
     */
    public function getAttributeForLinkTypeCode($linkTypeCode)
    {
        $attributeCode = preg_replace('(^' . preg_quote(self::CUSTOM_CODE_PREFIX) . ')', '', $linkTypeCode);
        return Mage::getResourceModel('catalog/eav_attribute')->load($attributeCode, 'attribute_code');
    }
    
    /**
     * 
     * @param String $attributeCode
     * @return type
     */
    private function _getLinkCodeFromAttributeCode($attributeCode)
    {
        return self::CUSTOM_CODE_PREFIX . $attributeCode;
    }
    
    /**
     * Creates a new product link type for the given attribute.
     * @param Mage_Catalog_Model_Resource_Eav_Attribute $attribute
     * @return Intellimage_CustomRelations_Model_Product_Link_Type
     */
    public function createForAttribute($attribute)
    {
        $linkType = Mage::getModel('customRelations/product_link_type');
        $linkType->setCode($this->_getLinkCodeFromAttributeCode($attribute->getAttributeCode()));
        $linkType->save();
        $linkAttribute = Mage::getModel('customRelations/product_link_attribute');
        $linkAttribute
            ->setLinkTypeId($linkType->getLinkTypeId())
            ->setData('product_link_attribute_code', 'position')
            ->setData('data_type', 'int')
            ->save();
        return $linkType;
    }

    /**
     * Deletes a product link for an attribute when its needed
     * @param Mage_Catalog_Model_Resource_Eav_Attribute $attribute
     * @return Intellimage_CustomRelations_Model_Product_Link_Type
     */
    public function deleteForAttributeIfNeeded($attribute)
    {
        $link = $this->existsForAttribute($attribute);
        if ($link) {
            return $link->delete();
        }
        return $this;
    }
}