<?php
/**
 * Intellimage
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to mauricioprado00@gmail.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade your
 * Intellimage extension to newer versions in the future. 
 * If you wish to customize your Intellimage extension to your
 * needs please refer to mauricioprado00@gmail.com for more information.
 *
 * @package     Intellimage_CustomRelations
 * @author      Hugo Mauricio Prado Macat
 * @copyright   2013
 * @email       mauricioprado00@gmail.com
 * @license     http://www.opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */



class Intellimage_CustomRelations_Helper_Import extends Intellimage_CustomRelations_Helper_Abstract
{
	const IMPORT_FILENAME = 'system/customrelationsimport/import_filename';
	const FTP_REMOTEFILE = 'system/customrelationsimport/ftp_remotefile';
	const FTP_DOWNLOAD = 'system/customrelationsimport/download_from_ftp';
	const FTP_APPEND_MODE = 'system/customrelationsimport/ftp_append_mode';
	const FTP_DAILY_IMPORT = 'system/customrelationsimport/daily_import';

	/**
	 * Import a file
	 * @param string $file 
	 * @param boolean $appendMode
	 * @param boolean $dowloadFromFtp
	 */
	public function runImport($file = null, $appendMode = false, $downloadFromFtp = false)
	{
		if (!isset($file)) {
			$file = Mage::getStoreConfig(self::IMPORT_FILENAME);
		}

		$path = Mage::getBaseDir("var") . DS . 'importexport' . DS . $file;

        if ($downloadFromFtp) {
        	if ($this->isVerbose()) {
        		echo "Retrieving import file from remote ftp location\n";
        	}
        	$this->_downloadFromFtp($path);
        }
		

		if (!file_exists($path)) {
			$message = 'Error importing custom relation. File does not exists ' . $path;
			throw new Exception($message);
		}

		if ($this->isVerbose()) {
			echo "Starting import file $path\n";
		}

        $model = Mage::getModel('customRelations/import_product_link');
        
        if ($appendMode) {
	        $model->setData($this->getRequest()->getParams());
	        $model->setData('append_mode', true);
        }
		
		Mage::log('import customrelations started');
        $model->importFile($path);
		Mage::log('import customrelations ends');


	}

	/**
	 * download from ftp
	 * @param string $localfile location where to download the file
	 */
	public function _downloadFromFtp($localfile)
	{
        $ftp = Mage::helper("customRelations/ftp");
        $searchfile = Mage::getStoreConfig(self::FTP_REMOTEFILE);
        $files = $ftp->listFiles($searchfile);

        if (!$files || !count($files)) {
        	throw new Exception("could not retrieve file from ftp: $searchfile");
        }

        $remotefile = array_shift($files);

        $res = $ftp->getFile($localfile, $remotefile);

        if (!$res) {
        	$message = "Could not retreive file $remotefile to $localfile";
        	throw new Exception($message, Zend_Log::ERR);
        }
	}

    /**
     * Runs the import cronjob
     */
	public function importCron()
	{
		if (Mage::getStoreConfig(self::FTP_DAILY_IMPORT)) {

			if ($this->isVerbose()) {
				echo "starting daily import\n";
			}

			$downloadFromFtp = (boolean) Mage::getStoreConfig(self::FTP_DOWNLOAD);
			$appendMode = (boolean) Mage::getStoreConfig(self::FTP_APPEND_MODE);

			if ($this->isVerbose()) {
				echo "finish daily import\n";
			}


			$this->runImport(null, $appendMode, $downloadFromFtp);
		} else {
			if ($this->isVerbose()) {
				echo "daily import disabled\n";
			}
		}
	}
}