<?php
/**
 * Intellimage
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to mauricioprado00@gmail.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade your
 * Intellimage extension to newer versions in the future. 
 * If you wish to customize your Intellimage extension to your
 * needs please refer to mauricioprado00@gmail.com for more information.
 *
 * @package     Intellimage_CustomRelations
 * @author      Hugo Mauricio Prado Macat
 * @copyright   2013
 * @email       mauricioprado00@gmail.com
 * @license     http://www.opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */



class Intellimage_CustomRelations_Helper_Data extends Mage_Core_Helper_Abstract
{
    const CONFIG_DEFAULT_TEMPLATE = 'design/customrelations/default_template';
    const CONFIG_EXCLUDE_QUOTE_ITEMS = 'catalog/customrelations/exclude_quote_items';
    const CONFIG_ADD_PRODUCT_ATTRIBUTES_AND_PRICES = 'catalog/customrelations/add_product_attributes_and_prices';
    const CONFIG_USE_CATEGORY_TEMPLATE = 'catalog/customrelations/use_category_template';
    const DEFAULT_TEMPLATE = 'catalog/product/list/any.phtml';

    public function getDefaultTemplate()
    {
        $template = Mage::getStoreConfig(self::CONFIG_DEFAULT_TEMPLATE);
        if (!$template) {
            $template = self::DEFAULT_TEMPLATE;
        }
        return $template;
    }
    
    /**
     * determines if the quote items must be excluded from listings
     * @return boolean
     */
    public function getExcludeQuoteItems()
    {
        return Mage::getStoreConfig(self::CONFIG_EXCLUDE_QUOTE_ITEMS) ? true : false;
    }
    
    public function getAddProductAttributesAndPrices()
    {
        return Mage::getStoreConfig(self::CONFIG_ADD_PRODUCT_ATTRIBUTES_AND_PRICES) ? true : false;
    }
    
    public function getUseCategoryTemplate()
    {
        return Mage::getStoreConfig(self::CONFIG_USE_CATEGORY_TEMPLATE) ? true : false;
    }
}