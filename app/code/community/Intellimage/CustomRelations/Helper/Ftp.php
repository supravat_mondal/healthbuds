<?php
/**
 * Intellimage
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to mauricioprado00@gmail.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade your
 * Intellimage extension to newer versions in the future. 
 * If you wish to customize your Intellimage extension to your
 * needs please refer to mauricioprado00@gmail.com for more information.
 *
 * @package     Intellimage_CustomRelations
 * @author      Hugo Mauricio Prado Macat
 * @copyright   2013
 * @email       mauricioprado00@gmail.com
 * @license     http://www.opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */



class Intellimage_CustomRelations_Helper_Ftp extends Intellimage_CustomRelations_Helper_Abstract
{
    const DOMAIN = 'system/customrelationsimport/ftp_domain';
    const PORT = 'system/customrelationsimport/ftp_port';
    const USER = 'system/customrelationsimport/ftp_user';
    const PASSWORD = 'system/customrelationsimport/ftp_password';
    const BASEDIR = 'system/customrelationsimport/ftp_basedir';
    
    private $_con = null;
    
    /**
     * Connects to the ftp server
     * @return resource or FALSE
     */
    private function _getConnection()
    {
        if (!isset($this->_con)) {
            $domain = Mage::getStoreConfig(self::DOMAIN);
            $port = Mage::getStoreConfig(self::PORT);
            $username = Mage::getStoreConfig(self::USER);
            $password = Mage::getStoreConfig(self::PASSWORD);
            
            // set up basic connection
            $this->_con = ftp_connect($domain, $port);
            
            // login with username and password
            $login_result = ftp_login($this->_con, $username, $password);
            if (!$login_result) {
            	throw new Exception("could not connect to ftp $domain $port $username $password");
                $this->_con = false;
            }
        }
        return $this->_con;
    }
    
    /**
     * test the ftp connection
     * @return boolean
     */
    public function testConnection()
    {
        return $this->_getConnection() ? true : false;
    }
    
    /**
     * Retrieves a file from the ftp server
     * @return boolean
     */
    public function getFile($localFile, $serverFile, $useBaseDir = true)
    {
        $con = $this->_getConnection();
        if ($con) {
            if ($useBaseDir) {
                $basedir = Mage::getStoreConfig(self::BASEDIR);
                $serverFile = rtrim(trim($basedir), '/') . '/' . $serverFile;
            }
            
            if ($this->isVerbose()) {
                echo "retriving ftp:$serverFile to local:$localFile\n";
            }
            return ftp_get($this->_con, $localFile, $serverFile, FTP_BINARY);
        }
        return false;
    }
    
    /**
     * Lists files of the specified glob on the ftp server
     * @param string $glob
     * @return array | NULL
     */
    public function listFiles($glob)
    {
        $subdir = $dir = dirname($glob);
        if ($dir === '.') {
            $dir = '';
        }
        $dir = Mage::getStoreConfig(self::BASEDIR) . '/' . $dir;
        $files = $this->listDirectory($dir);
        if ($files) {
            $expr = strtr(preg_quote(basename($glob)), array('\?' => '.', '\\*'=>'.*'));
            $return = array();
            foreach ($files as $file) {
                $basename = basename($file);
                if (preg_match('(^' . $expr . '$)', $basename)) {
                    $return[] = trim($subdir . '/' . $basename, '/');
                }
            }
            return $return;
        }
    }
    
    /**
     * Lists files of the specified directory on the ftp server
     * @param string $dir
     * @return array | FALSE
     */
    public function listDirectory($dir)
    {
        $con = $this->_getConnection();
        if ($con) {
            return ftp_nlist($this->_con, $dir);
        }
        return false;
    }
}