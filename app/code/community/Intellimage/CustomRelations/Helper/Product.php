<?php
/**
 * Intellimage
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to mauricioprado00@gmail.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade your
 * Intellimage extension to newer versions in the future. 
 * If you wish to customize your Intellimage extension to your
 * needs please refer to mauricioprado00@gmail.com for more information.
 *
 * @package     Intellimage_CustomRelations
 * @author      Hugo Mauricio Prado Macat
 * @copyright   2013
 * @email       mauricioprado00@gmail.com
 * @license     http://www.opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */



class Intellimage_CustomRelations_Helper_Product extends Mage_Core_Helper_Abstract
{
    /**
     * Retrieve related products identifiers of a given product link type
     * @param Mage_Catalog_Model_Product $product
     * @param string $productLinkTypeCode
     * @return array
     */
    public function getRelatedProductsIds($product, $productLinkTypeCode)
    {
        $ids = array();
        foreach ($this->getRelatedProducts($product, $productLinkTypeCode) as $product) {
            $ids[] = $product->getId();
        }
        return $ids;
    }

    /**
     * Retrieve array of related products by the given product link type
     * @param Mage_Catalog_Model_Product $product
     * @param string $productLinkTypeCode
     * @return array Mage_Catalog_Model_Product
     */
    public function getRelatedProducts($product, $productLinkTypeCode)
    {
        $products = array();
        $collection = $this->getRelatedProductCollection($product, $productLinkTypeCode);
        
        if ($collection) {
            foreach ($collection as $product) {
                $products[] = $product;
            }
        }
        
        return $products;
    }
    
    /**
     * Retrieve collection accesories product
     * @param Mage_Catalog_Model_Product $product
     * @param string $productLinkTypeCode
     * @return Mage_Catalog_Model_Product_Collection
     */
    public function getRelatedProductCollection($product, $productLinkTypeCode)
    {
        /** @var $link Mage_Catalog_Model_Product_Link */
        $link = Mage::helper('customRelations/link')->createProductLink($productLinkTypeCode);
        if ($link) {
            $collection = $link->getProductCollection()
                ->setIsStrongMode();
            $collection->setProduct($product);
            return $collection;
        }
        return null;
    }
}