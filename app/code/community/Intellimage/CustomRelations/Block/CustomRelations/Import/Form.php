<?php
/**
 * Intellimage
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to mauricioprado00@gmail.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade your
 * Intellimage extension to newer versions in the future. 
 * If you wish to customize your Intellimage extension to your
 * needs please refer to mauricioprado00@gmail.com for more information.
 *
 * @package     Intellimage_CustomRelations
 * @author      Hugo Mauricio Prado Macat
 * @copyright   2013
 * @email       mauricioprado00@gmail.com
 * @license     http://www.opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */



class Intellimage_CustomRelations_Block_CustomRelations_Import_Form extends Mage_Adminhtml_Block_Widget_Form
{

    /**
     * Initializes the form to import the csv file
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form(
            array(
                'id' => 'edit_form',
                'action' => $this->getUrl('*/*/save', array('id' => $this->getRequest()->getParam('id'))),
                'method' => 'post',
                'enctype' => 'multipart/form-data'
            )
        );
        
        $this->setForm($form);
        $fieldset = $form->addFieldset(
                'import_form', array('legend' => Mage::helper('customRelations')->__('Import Custom Relations'))
        );

        $fieldset->addField(
            'import_file', 'file', array(
                'label' => Mage::helper('customRelations')->__('Csv File'),
                'class' => 'required-entry',
                'required' => true,
                'name' => 'import_file',
            )
        );

        $fieldset->addField(
            'append_mode', 'checkbox', array(
                'label' => Mage::helper('customRelations')->__('Append Mode'),
                'name' => 'append_mode',
                'note' => '<span style="white-space: nowrap;">if you check this the previous relations of each product in the csv will be preserved, otherwise it will be cleared before adding them (example, if in the csv there is only an "accesory" for a product, then after running the import that product will have that only relation)</span>'
            )
        );

        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }

}