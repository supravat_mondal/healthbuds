<?php
/**
 * Intellimage
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to mauricioprado00@gmail.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade your
 * Intellimage extension to newer versions in the future. 
 * If you wish to customize your Intellimage extension to your
 * needs please refer to mauricioprado00@gmail.com for more information.
 *
 * @package     Intellimage_CustomRelations
 * @author      Hugo Mauricio Prado Macat
 * @copyright   2013
 * @email       mauricioprado00@gmail.com
 * @license     http://www.opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */



class Intellimage_CustomRelations_Block_Catalog_Product_List
    extends Mage_Catalog_Block_Product_List_Related
    implements Mage_Widget_Block_Interface
{
    const USE_CUSTOM_TEMPLATE = 'use_custom';
    
    /**
     * Product link type code
     * @var string
     */
    private $_productLinkTypeCode = null;
    
    protected $_defaultColumnCount = 4;
    
    public function _construct()
    {
        parent::_construct();
        if (!$this->hasTemplate()) {
            $this->setTemplate(Mage::helper('customRelations')->getDefaultTemplate());
        }
    }
    
    public function setProductLinkTypeCode($code)
    {
        $this->_productLinkTypeCode = $code;
        return $this;
    }
    
    public function getTitle()
    {
        if ($this->hasData('title')) {
            return $this->getData('title');
        }
        
        $code = preg_replace('(^custom_)', '', $this->_productLinkTypeCode);
        //$attribute = Mage::getModel('customRelations/product_link_attribute')->load($code, 'attribute_code');
        //$attributeModel = Mage::getModel('eav/entity_attribute')->loadByCode($entity_type, $attributeCode);
        $attribute = Mage::getModel('eav/entity_attribute')->load($code, 'attribute_code');
        if ($attribute->getId()) {
            return $attribute->getFrontendLabel();
        }
        
        return $this->__('Related Products');
    }
    
    protected function _prepareTemplate()
    {
        if ($this->getTemplate() === self::USE_CUSTOM_TEMPLATE) {
            $this->setTemplate($this->getCustomTemplate());
        }
    }
    
    protected function _beforeToHtml()
    {
        $this->_prepareTemplate();
        return parent::_beforeToHtml();
    }

    /**
     * Retrieve list toolbar HTML
     *
     * @return string
     */
    public function getToolbarHtml()
    {
        return $this->getChildHtml('toolbar');
    }
    
    public function getToolbarBlock()
    {
        return $this->getChild('toolbar');
    }

    protected function _prepareData()
    {
        
        $toolbar = $this->getToolbarBlock();

        // called prepare sortable parameters
        $this->_itemCollection = $this->_prepareCollection();

        if ($toolbar) {
            // use sortable parameters
            if ($orders = $this->getAvailableOrders()) {
                $toolbar->setAvailableOrders($orders);
            }
            if ($sort = $this->getSortBy()) {
                $toolbar->setDefaultOrder($sort);
            }
            if ($dir = $this->getDefaultDirection()) {
                $toolbar->setDefaultDirection($dir);
            }
            if ($modes = $this->getModes()) {
                $toolbar->setModes($modes);
            }

            // set collection to toolbar and apply sort
            $toolbar->setCollection($this->_itemCollection);

            $this->setChild('toolbar', $toolbar);
        }

        if ($this->_itemCollection) {
            $this->_itemCollection
                ->addAttributeToSelect('required_options')
                ->setPositionOrder()
                ->addStoreFilter();
            
            if (Mage::helper('customRelations')->getUseCategoryTemplate()) {
                $category = Mage::registry('current_category');
                if ($category) {
                    $template = $category->getData('custom_relation_template');
                    if ($template) {
                        $this->setTemplate($template);
                    }
                }
            }

            if (Mage::helper('catalog')->isModuleEnabled('Mage_Checkout')) {
                
                //determines if cart items must be excluded from the items list
                if (Mage::helper('customRelations')->getExcludeQuoteItems()) {
                    Mage::getResourceSingleton('checkout/cart')->addExcludeProductFilter(
                        $this->_itemCollection,
                        Mage::getSingleton('checkout/session')->getQuoteId()
                    );
                }
                
                if (Mage::helper('customRelations')->getAddProductAttributesAndPrices()) {
                    $this->_addProductAttributesAndPrices($this->_itemCollection);
                }
            }
    //        Mage::getSingleton('catalog/product_status')->addSaleableFilterToCollection($this->_itemCollection);
            
            Mage::getSingleton('catalog/product_visibility')
                ->addVisibleInCatalogFilterToCollection($this->_itemCollection);

            $this->_itemCollection->load();

            foreach ($this->_itemCollection as $product) {
                if (!Mage::helper('customRelations')->getAddProductAttributesAndPrices()) {
                    $product->load($product->getId());
                }
                $product->setDoNotUseCategoryId(true);
            }
        } else {
            $this->_itemCollection = new Varien_Data_Collection();
        }

        return $this;
    }
    
    public function getItemCollection()
    {
        return $this->_itemCollection;
    }
    
    public function getLoadedProductCollection()
    {
        return $this->_itemCollection;
    }
    
    private function _getProduct()
    {
        if ($this->hasIdPath() && strpos($this->getIdPath(), 'product/') === 0) {
            $id = intval(strpbrk($this->getIdPath(), '0123456789'));
            $product = Mage::getModel('catalog/product')->load($id);
        } elseif ($this->hasProductId()) {
            $product = Mage::getModel('catalog/product')->load($this->getProductId());
        } elseif ($this->hasProductSku()) {
            $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $this->getProductSku());
        } else {
            /* @var $product Mage_Catalog_Model_Product */
            $product = Mage::registry('product');
        }
        return $product;
    }
    
    private function _getProductLinkTypeCode()
    {
        if (isset($this->_productLinkTypeCode)) {
            return $this->_productLinkTypeCode;
        } 
        return $this->getData('product_link_type_code');
    }
    
    protected function _prepareCollection()
    {
        $product = $this->_getProduct();
        return Mage::helper('customRelations/product')
            ->getRelatedProductCollection($product, $this->_getProductLinkTypeCode());
    }
    
    public function getColumnCount()
    {
        $cc = intval($this->getData('column_count'));
        return $cc > 0 ? $cc : $this->_defaultColumnCount;
    }
    
    public function getRowCount()
    {
        return ceil(count($this->getItemCollection()->getItems())/$this->getColumnCount());
    }
}