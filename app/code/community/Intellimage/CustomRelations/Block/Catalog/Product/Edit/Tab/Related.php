<?php
/**
 * Intellimage
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to mauricioprado00@gmail.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade your
 * Intellimage extension to newer versions in the future. 
 * If you wish to customize your Intellimage extension to your
 * needs please refer to mauricioprado00@gmail.com for more information.
 *
 * @package     Intellimage_CustomRelations
 * @author      Hugo Mauricio Prado Macat
 * @copyright   2013
 * @email       mauricioprado00@gmail.com
 * @license     http://www.opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */



class Intellimage_CustomRelations_Block_Catalog_Product_Edit_Tab_Related
    extends Mage_Adminhtml_Block_Catalog_Product_Edit_Tab_Related
{
    private $_productLinkTypeCode = null;
    
    private $_jsObjectName = null;
    
    /**
     * Sets the product link type code to use to load the products
     * @param string $productLinkTypeCode
     * @return Intellimage_CustomRelations_Block_Catalog_Product_Edit_Tab_Related
     */
    public function setProductLinkTypeCode($productLinkTypeCode)
    {
        $this->_productLinkTypeCode = $productLinkTypeCode;
        $this->setId('custom_relation_' . $productLinkTypeCode . '_product_grid');
        return $this;
    }
    
    public function __construct()
    {
        parent::__construct();
        $this->setId('custom_relation_product_grid');
        $this->_productLinkTypeCode = Mage::app()->getRequest()->getParam("code");
    }
    
    public function getJsObjectName()
    {
        if (!isset($this->_jsObjectName)) {
#            $this->_jsObjectName = "jsCustomRelationsGrid" . uniqid();
            $this->_jsObjectName = "jsCustomRelationsGrid" . $this->_productLinkTypeCode;
        }
        return $this->_jsObjectName;
    }
    
    /**
     * Prepare collection
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        /** @var $link Mage_Catalog_Model_Product_Link */
        $link = Mage::helper('customRelations/link')->createProductLink($this->_productLinkTypeCode);
        $collection = $link->getProductCollection()
            ->setProduct($this->_getProduct())
            ->addAttributeToSelect('*');

        if ($this->isReadonly()) {
            $productIds = $this->_getSelectedProducts();
            if (empty($productIds)) {
                $productIds = array(0);
            }
            $collection->addFieldToFilter('entity_id', array('in' => $productIds));
        }

        $this->setCollection($collection);
        return Mage_Adminhtml_Block_Widget_Grid::_prepareCollection();
    }
    
    /**
     * Retrieve selected related products
     *
     * @return array
     */
    protected function _getSelectedProducts()
    {
        $products = $this->getProductsRelated();
        if (!is_array($products)) {
            $products = array_keys($this->getSelectedRelatedProducts());
        }
        return $products;
    }

    /**
     * Retrieve related products
     *
     * @return array
     */
    public function getSelectedRelatedProducts()
    {
        $products = array();
        foreach (Mage::helper('customRelations/product')
                    ->getRelatedProducts($this->_getProduct(), $this->_productLinkTypeCode) as $product) {
            $products[$product->getId()] = array('position' => $product->getPosition());
        }
        return $products;
    }

}