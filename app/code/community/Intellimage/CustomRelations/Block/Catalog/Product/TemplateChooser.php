<?php
/**
 * Intellimage
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to mauricioprado00@gmail.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade your
 * Intellimage extension to newer versions in the future. 
 * If you wish to customize your Intellimage extension to your
 * needs please refer to mauricioprado00@gmail.com for more information.
 *
 * @package     Intellimage_CustomRelations
 * @author      Hugo Mauricio Prado Macat
 * @copyright   2013
 * @email       mauricioprado00@gmail.com
 * @license     http://www.opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */



class Intellimage_CustomRelations_Block_Catalog_Product_TemplateChooser
    extends Mage_Adminhtml_Block_Widget_Form_Renderer_Fieldset_Element
    implements Varien_Data_Form_Element_Renderer_Interface
{
    
    const CONFIG_WIDGET_TEMPLATE_OPTIONS = 'intellimage/customRelations/widget/list/templates';
    
    public function render(Varien_Data_Form_Element_Abstract $element)
    {
        $select = new Varien_Data_Form_Element_Select();
        $select->addData($element->getData());
        $select->setValues($this->_getValues($element->getValues()));
        $select->setForm($element->getForm());
        $this->_element = $select;
        return $this->toHtml();
    }
    
    protected function _getValues($predefinedValues)
    {
        $values = is_array($predefinedValues) ? $predefinedValues : array();
        return array_merge($values, $this->_loadValuesFromConfig());
    }
    
    public function _loadValuesFromConfig()
    {
        $values = array();
        $templates = Mage::getStoreConfig(self::CONFIG_WIDGET_TEMPLATE_OPTIONS);
        if ($templates) {
            foreach ($templates as $name => $templateConfig) {
                $values[] = array(
                    'value' => (string) $templateConfig['value'],
                    'label' => (string) $templateConfig['label']
                );
            }
        }
        
        return $values;
    }
}