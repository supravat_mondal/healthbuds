<?php
/**
 * Intellimage
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to mauricioprado00@gmail.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade your
 * Intellimage extension to newer versions in the future. 
 * If you wish to customize your Intellimage extension to your
 * needs please refer to mauricioprado00@gmail.com for more information.
 *
 * @package     Intellimage_CustomRelations
 * @author      Hugo Mauricio Prado Macat
 * @copyright   2013
 * @email       mauricioprado00@gmail.com
 * @license     http://www.opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */



class Intellimage_CustomRelations_Block_Widget_ElementRenderer
    extends Mage_Adminhtml_Block_Widget_Form_Renderer_Fieldset_Element
    implements Varien_Data_Form_Element_Renderer_Interface
{
    
    public function render(Varien_Data_Form_Element_Abstract $element)
    {
        $select = new Varien_Data_Form_Element_Select();
        $select->addData($element->getData());
        $select->setValues($this->_getValues());
        $select->setForm($element->getForm());
        $this->_element = $select;
        return $this->toHtml();
    }
    
    protected function _getValues()
    {
        $values = array();
        $collection = $this->_prepareCollection();
        foreach ($collection as $attribute) {
            $linkType = Mage::helper('customRelations/link')->existsForAttribute($attribute);
            if ($linkType) {
                $label = $attribute->getStoreLabel();
                /**
                 * fix magento 1.5 and lower
                 */
                if (!$label) {
                    $label = $attribute->getFrontendLabel();
                }
                $values[$linkType->getCode()] = $label;
            }
        }
        return $values;
    }
    
    /**
     * 
     * @return Mage_Adminhtml_Block_Catalog_Product_Attribute_Grid
     */
    protected function _prepareCollection()
    {
        return Mage::getResourceModel('catalog/product_attribute_collection')
            ->addFieldToFilter('is_product_link_type', '1')
            ->addVisibleFilter();
    }
}