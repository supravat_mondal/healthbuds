<?php
/**
 * Intellimage
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to mauricioprado00@gmail.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade your
 * Intellimage extension to newer versions in the future. 
 * If you wish to customize your Intellimage extension to your
 * needs please refer to mauricioprado00@gmail.com for more information.
 *
 * @package     Intellimage_CustomRelations
 * @author      Hugo Mauricio Prado Macat
 * @copyright   2013
 * @email       mauricioprado00@gmail.com
 * @license     http://www.opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */



require_once  Mage::getModuleDir('controllers', 'Mage_Adminhtml') . DS . 'Catalog' . DS . 'ProductController.php';

/*
 * http://mgt8.localhost.com/index.php/customRelations/related/tab/code/custom_accesories/id/160
 */
    
class Intellimage_CustomRelations_Adminhtml_CustomRelationsController 
    extends Mage_Adminhtml_Catalog_ProductController //Mage_Core_Controller_Varien_Action 
{
    /**
     * Get related products grid and serializer block
     */
    public function tabAction()
    {
        $this->_initProduct();
        $this->loadLayout();
        $productLinkTypeCode = $this->getRequest()->getParam("code");
        $this->getLayout()->getBlock('catalog.product.edit.tab.related')
            ->setProductLinkTypeCode($productLinkTypeCode)
            ->setGridUrl(
                Mage::getUrl(
                    '*/customRelations/tabGrid/',
                    array('_current' => true)
                )
            )
            ->setProductsRelated($this->getRequest()->getPost('products_related', null));
        
        if ($serializer = $this->getLayout()->getBlock('related_grid_serializer')) {
            $serializer->setInputElementName('links[' . $productLinkTypeCode . ']');
        }   
        
        $this->renderLayout();
    }
    
    /**
     * Get related products grid and serializer block
     */
    public function tabGridAction()
    {
        return $this->tabAction();
    }
}
