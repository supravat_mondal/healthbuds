<?php
/**
 * Intellimage
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to mauricioprado00@gmail.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade your
 * Intellimage extension to newer versions in the future. 
 * If you wish to customize your Intellimage extension to your
 * needs please refer to mauricioprado00@gmail.com for more information.
 *
 * @package     Intellimage_CustomRelations
 * @author      Hugo Mauricio Prado Macat
 * @copyright   2013
 * @email       mauricioprado00@gmail.com
 * @license     http://www.opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */



class Intellimage_CustomRelations_Adminhtml_CustomRelations_ImportController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Loads the import form
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->_setActiveMenu('system/convert/import_custom_relations');

        $this->_addBreadcrumb(
            Mage::helper('adminhtml')->__('Import Custom Relations'),
            Mage::helper('adminhtml')->__('Import Custom Relations')
        );

        $this->_addBreadcrumb(
            Mage::helper('adminhtml')->__('Import Custom Relations'), 
            Mage::helper('adminhtml')->__('Import Custom Relations')
        );

        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
        
//            $this->_addContent($this->getLayout()->createBlock('customRelations/customRelations_import_edit'))
//                ->_addLeft($this->getLayout()->createBlock('customRelations/customRelations_import_edit_tabs'));

        $this->renderLayout();
            
    }
    
    /**
     * receives and process the file
     */
    public function saveAction()
    {
        try {
            header('content-type:text/plain');
            /** @var $model Intellimage_CustomRelations_Model_Import_Product_Link */
            $model = Mage::getModel('customRelations/import_product_link');
            
            $model->setData($this->getRequest()->getParams());

            $model->importSource();
            
        } catch (Mage_Core_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        } catch (Exception $e) {
            Mage::logException($e);
            $this->_getSession()->addError($this->__('Could not import file. ' .$e->getMessage() ));
        }
        
        if ($model->getErrorCount()) {
            $this->_getSession()->addError("{$model->getErrorCount()} relations could not be imported, see customRelations_error.log");
        }

        if ($model->getImportedCount()) {
            $this->_getSession()->addSuccess("{$model->getImportedCount()} relations imported");
        }

        $this->_redirect('*/*/index');
    }
}
