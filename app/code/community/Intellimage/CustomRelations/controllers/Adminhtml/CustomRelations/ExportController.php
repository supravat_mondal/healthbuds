<?php
/**
 * Intellimage
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to mauricioprado00@gmail.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade your
 * Intellimage extension to newer versions in the future. 
 * If you wish to customize your Intellimage extension to your
 * needs please refer to mauricioprado00@gmail.com for more information.
 *
 * @package     Intellimage_CustomRelations
 * @author      Hugo Mauricio Prado Macat
 * @copyright   2013
 * @email       mauricioprado00@gmail.com
 * @license     http://www.opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */



include 'Mage/ImportExport/controllers/Adminhtml/ExportController.php';

class Intellimage_CustomRelations_Adminhtml_CustomRelations_ExportController 
    extends Mage_ImportExport_Adminhtml_ExportController
{
    /**
     * Loads the export form
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->_setActiveMenu('system/convert/export_custom_relations');

        $this->_addBreadcrumb(
            Mage::helper('adminhtml')->__('Export Custom Relations'),
            Mage::helper('adminhtml')->__('Export Custom Relations')
        );

        $this->_addBreadcrumb(
            Mage::helper('adminhtml')->__('Export Custom Relations'), 
            Mage::helper('adminhtml')->__('Export Custom Relations')
        );

        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
        
//            $this->_addContent($this->getLayout()->createBlock('customRelations/customRelations_export_edit'))
//                ->_addLeft($this->getLayout()->createBlock('customRelations/customRelations_export_edit_tabs'));

        $this->renderLayout();
            
    }
    
    /**
     * receives and process the file
     */
    public function saveAction()
    {
        header('content-type: text/plain');
        
        try {
            /** @var $model Intellimage_CustomRelations_Model_Export_Product_Link */
            $model = Mage::getModel('customRelations/export_product_link');

            $model->setData($this->getRequest()->getParams());
            
            return $this->_prepareDownloadResponse(
                $model->getFileName(),
                $model->export(),
                $model->getContentType()
            );
        } catch (Mage_Core_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        } catch (Exception $e) {
            Mage::logException($e);
            $this->_getSession()->addError($this->__('No valid data sent ' .$e->getMessage() ));
        }

        $this->_redirect('*/*/index');
    }
}
