<?php
/**
 * Intellimage
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to mauricioprado00@gmail.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade your
 * Intellimage extension to newer versions in the future. 
 * If you wish to customize your Intellimage extension to your
 * needs please refer to mauricioprado00@gmail.com for more information.
 *
 * @package     Intellimage_CustomRelations
 * @author      Hugo Mauricio Prado Macat
 * @copyright   2013
 * @email       mauricioprado00@gmail.com
 * @license     http://www.opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */



class Intellimage_CustomRelations_Model_Export_Product_Link extends Varien_Object
{
    const CONFIG_KEY_FORMATS  = 'global/importexport/export_file_formats';
    
    /**
     * @var Mage_ImportExport_Model_Export_Entity_Abstract 
     */
    private $_writer = null;
    
    private $_connection = null;
    
    /**
     * Constructor.
     *
     * @return void
     */
    public function __construct()
    {
        //$entityCode = $this->getEntityTypeCode();
        //$this->_entityTypeId = Mage::getSingleton('eav/config')->getEntityType($entityCode)->getEntityTypeId();
        $this->_connection   = Mage::getSingleton('core/resource')->getConnection('write');
    }
    
    /**
     * Prepare product links
     *
     * @param  array $linkTypes
     * @param  array $productIds
     * @return Varien_Db_Statement_Pdo_Mysql
     */
    protected function _prepareLinks($linkTypes, $productIds)
    {
        $resource = Mage::getSingleton('core/resource');
        $adapter = $this->_connection;
        $select = $adapter->select()
            ->from(
                array('cpl' => $resource->getTableName('catalog/product_link')),
                array(
                    //'cpl.product_id', 
                    'sku' => 'cpe.sku', 
                    'linked_sku' => 'cple.sku', 
                    //'cpl.link_type_id',
                    'link_type_code' => 'cplt.code',
                    'position' => 'cplai.value', 
                    //'default_qty' => 'cplad.value',
                )
            )
            ->joinLeft(
                array('cpe' => $resource->getTableName('catalog/product')),
                '(cpe.entity_id = cpl.product_id)',
                array()
            )
            ->joinLeft(
                array('cple' => $resource->getTableName('catalog/product')),
                '(cple.entity_id = cpl.linked_product_id)',
                array()
            )
            ->joinLeft(
                array('cpla' => $resource->getTableName('catalog/product_link_attribute')),
                $adapter->quoteInto(
                    '(cpla.link_type_id = cpl.link_type_id AND cpla.product_link_attribute_code = ?)',
                    'position'
                ),
                array()
            )
            ->joinLeft(
                array('cplaq' => $resource->getTableName('catalog/product_link_attribute')),
                $adapter->quoteInto(
                    '(cplaq.link_type_id = cpl.link_type_id AND cplaq.product_link_attribute_code = ?)',
                    'qty'
                ),
                array()
            )
            ->joinLeft(
                array('cplt' => $resource->getTableName('catalog/product_link_type')),
                $adapter->quoteInto(
                    '(cplt.link_type_id = cpl.link_type_id)',
                    'qty'
                ),
                array()
            )
            ->joinLeft(
                array('cplai' => $resource->getTableName('catalog/product_link_attribute_int')),
                '(cplai.link_id = cpl.link_id AND cplai.product_link_attribute_id = cpla.product_link_attribute_id)',
                array()
            )
            ->joinLeft(
                array('cplad' => $resource->getTableName('catalog/product_link_attribute_decimal')),
                '(cplad.link_id = cpl.link_id AND cplad.product_link_attribute_id = cplaq.product_link_attribute_id)',
                array()
            );

        if (!empty($linkTypes)) {
            $select->where('cpl.link_type_id IN (?)', $linkTypes);
        }
        
        if (!empty($productIds)) {
            $select->where('cpl.product_id IN (?)', $productIds);
        }
        
        return $adapter->query($select);
    }
    
    /**
     * Inner writer object getter.
     *
     * @throws Exception
     * @return Mage_ImportExport_Model_Export_Adapter_Abstract
     */
    public function getWriter()
    {
        if (!$this->_writer) {
            /**
             * set the writer instance
             */
            $validWriters = Mage_ImportExport_Model_Config::getModels(self::CONFIG_KEY_FORMATS);
            $this->_writer = Mage::getModel($validWriters['csv']['model']);
            //Mage::throwException(Mage::helper('importexport')->__('No writer specified'));
        }
        return $this->_writer;
    }

    /**
     * Writer model setter.
     *
     * @param Mage_ImportExport_Model_Export_Adapter_Abstract $writer
     * @return Mage_ImportExport_Model_Export_Entity_Abstract
     */
    public function setWriter(Mage_ImportExport_Model_Export_Adapter_Abstract $writer)
    {
        $this->_writer = $writer;

        return $this;
    }
    
    /**
     * @return string
     */
    public function _export()
    {
        $writer = $this->getWriter();
        
        $headerCols = array(
            'sku',
            'linked_sku', 
            'link_type_code',
            'position',
        );
        
        $writer->setHeaderCols($headerCols);
                
        $stmt = $this->_prepareLinks($this->getLinkTypeIds(), $this->getProductIds());
        
        while ($linksRow = $stmt->fetch()) {
            $writer->writeRow($linksRow);
        }
        
        return $writer->getContents();
    }

    /**
     * MIME-type for 'Content-Type' header.
     *
     * @return string
     */
    public function getContentType()
    {
        return $this->getWriter()->getContentType();
    }
    
    /**
     * Return file name for downloading.
     *
     * @return string
     */
    public function getFileName()
    {
        return 'customRelations_' . date('Ymd_His') .  '.' . $this->getWriter()->getFileExtension();
    }
    
    /**
     * Export data.
     *
     * @throws Mage_Core_Exception
     * @return string
     */
    public function export()
    {
        Mage::log(Mage::helper('customRelations')->__('Begin export of custom relations'), Zend_Log::INFO, 'custom_relations.log');

        /**
         * export the data
         */
        $result = $this->_export();
        
        $countRows = substr_count(trim($result), "\n");
        if (!$countRows) {
            Mage::throwException(
                Mage::helper('importexport')->__('There is no data for export')
            );
        }
        if ($result) {
            Mage::log(Mage::helper('customRelations')->__('Exported %s rows.', $countRows), Zend_Log::INFO, 'custom_relations.log');
            Mage::log(Mage::helper('customRelations')->__('Export has been done.'), Zend_Log::INFO, 'custom_relations.log');
        }
        return $result;
    }
}