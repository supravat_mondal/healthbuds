<?php
/**
 * Intellimage
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to mauricioprado00@gmail.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade your
 * Intellimage extension to newer versions in the future. 
 * If you wish to customize your Intellimage extension to your
 * needs please refer to mauricioprado00@gmail.com for more information.
 *
 * @package     Intellimage_CustomRelations
 * @author      Hugo Mauricio Prado Macat
 * @copyright   2013
 * @email       mauricioprado00@gmail.com
 * @license     http://www.opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */



class Intellimage_CustomRelations_Model_Feed
{
    const XML_USE_HTTPS_PATH    = 'system/{mod}_notification/use_https';
    const XML_FEED_URL_PATH     = 'system/{mod}_notification/feed_url';
    const XML_FREQUENCY_PATH    = 'system/{mod}_notification/frequency';
    const CACHE_LAST_UPDATE_PATH  = 'system/{mod}_notification/last_update';

    /**
     * Feed url
     *
     * @var string
     */
    protected $_feedUrl;
    
    /**
     * module name
     * @var string
     */
    protected $_moduleName;
    
    public function controllerActionPredispatch()
    {
        if (Mage::getSingleton('admin/session')->isLoggedIn()) {
            $this->checkUpdate();
        }
    }
    
    protected function getConst($value)
    {
        $module = strtolower($this->_getModuleName());
        return str_replace('{mod}', $module, $value);
    }
    
    public function checkUpdate()
    {
        if (($this->getFrequency() + $this->getLastUpdate()) > time()) {
            return $this;
        }
        $this->_update();
    }
    
    protected function getStoreConfig($name)
    {
        return Mage::getStoreConfig($this->getConst($name));
    }
    
    protected function setStoreConfig($name, $value)
    {
        $config = Mage::getModel('core/config');
        /* @var $config Mage_Core_Model_Config */
        $config->saveConfig($this->getConst($name), $value);
    }
    
    protected function _getFrequencyConfig()
    {
        return $this->getStoreConfig(self::XML_FREQUENCY_PATH);
    }
    
    protected function _getCacheLastUpdatePath()
    {
        return $this->getConst(self::CACHE_LAST_UPDATE_PATH);
    }
    
    protected function _getFeedUrlPathConfig()
    {
        return $this->getStoreConfig(self::XML_FEED_URL_PATH);
    }
    
    protected function _setFeedUrlPathConfig($value)
    {
        return $this->setStoreConfig(self::XML_FEED_URL_PATH, $value);
    }
    
    /**
     * Retrieve Update Frequency
     *
     * @return int
     */
    public function getFrequency()
    {
        return $this->_getFrequencyConfig() * 3600;
    }
    
    /**
     * Retrieve Last update time
     *
     * @return int
     */
    public function getLastUpdate()
    {
        return Mage::app()->loadCache($this->_getCacheLastUpdatePath());
    }

    /**
     * Set last update time (now)
     *
     * @return Mage_AdminNotification_Model_Feed
     */
    public function setLastUpdate()
    {
        Mage::app()->saveCache(time(), $this->_getCacheLastUpdatePath());
        return $this;
    }

    /**
     * Retrieve feed url
     *
     * @return string
     */
    public function getFeedUrl()
    {
        if (is_null($this->_feedUrl)) {
            $this->_feedUrl = $this->_getFeedUrlPathConfig();
        }
        return $this->_feedUrl . '?' . time();
    }

    protected function _updateFeedUrl($url) 
    {
        if ($url !== $this->getFeedUrl()) {
            $this->_setFeedUrlPathConfig($url);
        }
    }
    
    /**
     * Retrieve DB date from RSS date
     *
     * @param string $rssDate
     * @return string YYYY-MM-DD YY:HH:SS
     */
    public function getDate($rssDate)
    {
        return gmdate('Y-m-d H:i:s', strtotime($rssDate));
    }

    public function _update()
    {
        $feedXml = $this->getFeedData();

        if ($feedXml && $feedXml->channel && $feedXml->channel->item) {
            foreach ($feedXml->channel->item as $item) {
                $action = isset($item->action) ? (string) $item->action : '';
                if ($action) {
                    $this->_action($action, $item);
                } else {
                    $feedData[] = array(
                        'severity'      => (int)$item->severity,
                        'date_added'    => $this->getDate((string)$item->pubDate),
                        'title'         => (string)$item->title,
                        'description'   => (string)$item->description,
                        'url'           => (string)$item->link,
                    );
                }
            }
            if ($feedData) {
                Mage::getModel('adminnotification/inbox')->parse(array_reverse($feedData));
            }
        }
        $this->setLastUpdate();
    }
    
    protected function _action($action, $item)
    {
        switch ($action) {
            case 'update_url':
                $this->_updateFeedUrl((string) $item->url);
                break;
        }
    }
    
    /**
     * Retrieve feed data as XML element
     *
     * @return SimpleXMLElement
     */
    public function getFeedData()
    {
        $url = $this->getFeedUrl();
        $curl = new Varien_Http_Adapter_Curl();
        $curl->setConfig(
            array(
                'timeout'   => 2,
                'header' => 0
            )
        );
        $curl->addOption(CURLOPT_FOLLOWLOCATION, true);
        $curl->write(Zend_Http_Client::GET, $url, '1.0');
        $data = $curl->read();
        if ($data === false) {
            return false;
        }

        $curl->close();
        try {
            $xml  = new SimpleXMLElement($data);
        }
        catch (Exception $e) {
            return false;
        }

        return $xml;
    }
    
    /**
     * Retrieve helper module name
     *
     * @return string
     */
    protected function _getModuleName()
    {
        if (!$this->_moduleName) {
            $class = get_class($this);
            $this->_moduleName = substr($class, 0, strpos($class, '_Model'));
        }
        return $this->_moduleName;
    }
}