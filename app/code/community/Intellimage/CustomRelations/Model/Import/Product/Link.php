<?php
/**
 * Intellimage
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to mauricioprado00@gmail.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade your
 * Intellimage extension to newer versions in the future. 
 * If you wish to customize your Intellimage extension to your
 * needs please refer to mauricioprado00@gmail.com for more information.
 *
 * @package     Intellimage_CustomRelations
 * @author      Hugo Mauricio Prado Macat
 * @copyright   2013
 * @email       mauricioprado00@gmail.com
 * @license     http://www.opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */



class Intellimage_CustomRelations_Model_Import_Product_Link extends Varien_Object
{
    const FIELD_NAME_SOURCE_FILE = 'import_file';
    
    /**
     * DB connection.
     *
     * @var Varien_Adapter_Interface
     */
    protected $_connection;
    
    /**
     * if append mode is off then the product relation 
     * is cleared before inserting a new one.
     * @var boolean
     */
    protected $_appendMode = false;
    
    /**
     * amount of errors during import
     * @var int
     */
    protected $_errors = 0;
    
    /**
     * amount of successful imports
     * @var int
     */
    protected $_imported = 0;
    
    /**
     * Links attribute name-to-link type ID.
     *
     * @var array
     */
    protected $_linkNameToId = array(
        '_links_related_'   => Mage_Catalog_Model_Product_Link::LINK_TYPE_RELATED,
        '_links_crosssell_' => Mage_Catalog_Model_Product_Link::LINK_TYPE_CROSSSELL,
        '_links_upsell_'    => Mage_Catalog_Model_Product_Link::LINK_TYPE_UPSELL,
        '_accesories_'    => 6,
    );
    
    /**
     * Constructor.
     *
     * @return void
     */
    public function __construct()
    {
//        $entityType = Mage::getSingleton('eav/config')->getEntityType($this->getEntityTypeCode());
//        $this->_entityTypeId    = $entityType->getEntityTypeId();
//        $this->_dataSourceModel = Mage_ImportExport_Model_Import::getDataSourceModel();
        $this->_connection      = Mage::getSingleton('core/resource')->getConnection('write');
    }
    
    /**
     * 
     * @param Mage_ImportExport_Model_Import_Adapter_Abstract $sourceAdapter
     * @see Mage_ImportExport_Model_Import_Entity_Product::_saveLinks
     */
    protected function _process($sourceAdapter)
    {
        $resource       = Mage::getResourceModel('catalog/product_link');
        $mainTable      = $resource->getMainTable();
        $productTable   = Mage::getModel('catalog/product')->getResource()->getEntityTable();
        $cleared = array();
        
        $append = $this->hasAppendMode() ? true : $this->_appendMode;
        
        /** @var Varien_Db_Adapter_Interface $adapter */
        $adapter = $this->_connection;
        
        $positionAttrId = array();
        $linkTypeIds = array();
        
        /**
         * Get all custom link types
         */
        $linkTypes = Mage::getModel('customRelations/source_link_type_customId')->toOptionArray();
        
        /**
         * Initialize link type position attribute id array
         */
        foreach ($linkTypes as $linkType) {
            $linkTypeId = $linkType['value'];
            $linkTypeCode = $linkType['code'];
            
            $select = $adapter->select()
                ->from(
                    $resource->getTable('catalog/product_link_attribute'),
                    array('id' => 'product_link_attribute_id')
                )
                ->where('link_type_id = :link_type_id AND product_link_attribute_code = :position');
            $bind = array(
                ':link_type_id' => $linkTypeId,
                ':position' => 'position'
            );
            $positionAttrId[$linkTypeCode] = $adapter->fetchOne($select, $bind);
            $linkTypeIds[$linkTypeCode] = $linkTypeId;

        }
        
        $nextLinkId = Mage::getResourceHelper('importexport')->getNextAutoincrement($mainTable);
        
        $this->_errors = 0;
        $this->_imported = 0;
        
        foreach($sourceAdapter as $row) {
            $sku = $row['sku'];
            $linkedSku = $row['linked_sku'];
            $linkTypeCode = $row['link_type_code'];
            $position = $row['position'];
            $linkTypeId = $linkTypeIds[$linkTypeCode];
            
            $newProducts = $adapter->fetchPairs($this->_connection->select()
                ->from($productTable, array('sku', 'entity_id'))
                ->where('sku IN (?)', array($sku, $linkedSku))
            );
            
            if (!isset($newProducts[$sku]) || !isset($newProducts[$linkedSku])) {
                $this->_errors++;
                Mage::log("could not find product/s $sku $linkedSku", Zend_Log::ERR, 'customRelations_error.log');
                continue;
            }
            
            $productId = $newProducts[$sku];
            $linkedProductId = $newProducts[$linkedSku];
            
            if (!$append && !isset($cleared[$productId][$linkTypeId])) {
                $adapter->delete(
                    $mainTable,
                    $adapter->quoteInto('product_id IN (?)', array($productId)) . ' AND ' . $adapter->quoteInto('link_type_id = ?', array($linkTypeId))
                );
                $cleared[$productId][$linkTypeId] = true;
            }
            
            $linkRows[] = array(
                'link_id'           => $nextLinkId,
                'product_id'        => $productId,
                'linked_product_id' => $linkedProductId,
                'link_type_id'      => $linkTypeIds[$linkTypeCode]
            );
            
            $adapter->insertOnDuplicate(
                    $mainTable,
                    $linkRows,
                    array('link_id')
                );
            $adapter->changeTableAutoIncrement($mainTable, $nextLinkId);
            
            if (!empty($position)) {
                $positionRows[] = array(
                    'link_id'                   => $nextLinkId,
                    'product_link_attribute_id' => $positionAttrId[$linkTypeCode],
                    'value'                     => $position
                );
                $adapter->insertOnDuplicate(
                    $resource->getAttributeTypeTable('int'),
                    $positionRows,
                    array('value')
                );
            }
            
            $nextLinkId++;
            $this->_imported ++;
        }
    }
    
    /**
     * Import/Export working directory (source files, result files, lock files etc.).
     *
     * @return string
     */
    public static function getWorkingDir()
    {
        return Mage::getBaseDir('var') . DS . 'importexport' . DS;
    }
    
    /**
     * Move uploaded file and create source adapter instance.
     *
     * @throws Mage_Core_Exception
     * @return string Source file path
     */
    public function uploadSource()
    {
        $entity = 'customRelations';
        $uploader  = Mage::getModel('core/file_uploader', self::FIELD_NAME_SOURCE_FILE);
        $uploader->skipDbProcessing(true);
        $result    = $uploader->save(self::getWorkingDir());
        $extension = pathinfo($result['file'], PATHINFO_EXTENSION);
        $uploadedFile = $result['path'] . $result['file'];
        
        if (!$extension) {
            unlink($uploadedFile);
            Mage::throwException(Mage::helper('importexport')->__('Uploaded file has no extension'));
        }
        $sourceFile = self::getWorkingDir() . $entity;

        $sourceFile .= '.' . $extension;

        if(strtolower($uploadedFile) != strtolower($sourceFile)) {
            if (file_exists($sourceFile)) {
                unlink($sourceFile);
            }

            if (!@rename($uploadedFile, $sourceFile)) {
                Mage::throwException(Mage::helper('importexport')->__('Source file moving failed'));
            }
        }
        return $sourceFile;
    }

    /**
     * Returns source adapter object.
     *
     * @param string $sourceFile Full path to source file
     * @return Mage_ImportExport_Model_Import_Adapter_Abstract
     */
    protected function _getSourceAdapter($sourceFile)
    {
        return Mage_ImportExport_Model_Import_Adapter::findAdapterFor($sourceFile);
    }
    
    /**
     * Returns source adapter object.
     *
     * @param string $sourceFile Full path to source file
     * @return Mage_ImportExport_Model_Import_Adapter_Abstract
     */
    public function importSource()
    {
        $sourceFile = $this->uploadSource();

        return $this->importFile($sourceFile);
    }

    /**
     * Returns source adapter object.
     *
     * @param string $sourceFile Full path to source file
     * @return Mage_ImportExport_Model_Import_Adapter_Abstract
     */
    public function importFile($sourceFile)
    {
        // trying to create source adapter for file and catch possible exception to be convinced in its adequacy
        try {
            $sourceAdapter = $this->_getSourceAdapter($sourceFile);

            $this->_process($sourceAdapter);
            
        } catch (Exception $e) {
            unlink($sourceFile);
            Mage::throwException($e->getMessage());
        }
    }
    
    /**
     * @return int
     */
    public function getErrorCount()
    {
        return $this->_errors;
    }
    
    /**
     * @return int
     */
    public function getImportedCount()
    {
        return $this->_imported;
    }
}
