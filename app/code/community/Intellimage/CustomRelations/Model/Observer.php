<?php
/**
 * Intellimage
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to mauricioprado00@gmail.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade your
 * Intellimage extension to newer versions in the future. 
 * If you wish to customize your Intellimage extension to your
 * needs please refer to mauricioprado00@gmail.com for more information.
 *
 * @package     Intellimage_CustomRelations
 * @author      Hugo Mauricio Prado Macat
 * @copyright   2013
 * @email       mauricioprado00@gmail.com
 * @license     http://www.opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */



class Intellimage_CustomRelations_Model_Observer
{

    /**
     * Handles the event core_block_abstract_prepare_layout_after
     */
    public function onCoreBlockAbstractPrepareLayoutAfter($observer)
    {
        $block = $observer->getBlock();
        if ($block) {
            $class = get_class($block);
            if ($class === 'Mage_Adminhtml_Block_Catalog_Product_Edit_Tabs') {
                $this->_prepareLayoutAfterCatalogProductEditTabs($block);
            }
        }
    }
    
    /**
     * handles the event adminhtml_catalog_product_attribute_edit_prepare_form
     */
    public function onCatalogProductAttributeEditPrepareForm($observer)
    {
        /* @var $form Varien_Data_Form */
        $form = $observer->getForm();
        /* @var $attribute Mage_Catalog_Model_Resource_Eav_Attribute */
        $attribute = $observer->getAttribute();
        $this->_prepareAttributeForm($form);
    }
    
    /**
     * Handles the event model_save_commit_after
     */
    public function onModelSaveCommitAfter($observer)
    {
        $object = $observer->getObject();
        if ($object) {
            $class = get_class($object);
            if ($class === 'Mage_Catalog_Model_Resource_Eav_Attribute') {
                $this->_modelSaveCommitAfterResourceEavAttribute($object);
            }
        }
    }
    
    /**
     * Handles the event model_delete_before
     */
    public function onModelDeleteBefore($observer)
    {
        $object = $observer->getObject();
        if ($object) {
            $class = get_class($object);
            if ($class === 'Mage_Catalog_Model_Resource_Eav_Attribute') {
                $this->_modelDeleteBeforeResourceEavAttribute($object);
            }
        }
    }
   
   /**
    * Initializes the tabs block
    * @param Mage_Adminhtml_Block_Catalog_Product_Edit_Tabs $block
    */
   protected function _prepareLayoutAfterCatalogProductEditTabs($block)
   {
        $product = $block->getProduct();
        if ($product) {
            if (!($setId = $product->getAttributeSetId())) {
                $setId = Mage::app()->getRequest()->getParam('set', null);
            }
            
            if ($setId) {
                $attributes = $product->getAttributes();
                foreach ($attributes as $attribute) {
                    if ($attribute->getIsProductLinkType()) {
                        $attributeCode = $attribute->getAttributeCode();
                        $link = Mage::helper('customRelations/link')
                            ->existsForAttribute($attribute);
                        if ($link) {
                            $block->addTab(
                                $link->getCode(), //getFrontendLabel()
                                array(
                                    'label'     => $attribute->getStoreLabel(),
                                    'url'       => $block->getUrl(
                                        '*/customRelations/tab/',
                                        array('code' => $link->getCode(), '_current' => true)
                                    ),
                                    'class'     => 'ajax',
                                )
                            );
                        }
                    }
                }
            }
        }
   }
   
   /**
    * 
    * @param Varien_Data_Form $block
    */
   protected function _prepareAttributeForm($form)
   {
        $yesnoSource = Mage::getModel('adminhtml/system_config_source_yesno')->toOptionArray();
        $fieldset = $form->getElement('base_fieldset');
        $fieldset->addField(
            'is_product_link_type', 'select', 
            array(
                'name'     => 'is_product_link_type',
                'label'    => Mage::helper('customRelations')->__('Is Product Link Type'),
                'title'    => Mage::helper('customRelations')->__('Is Product Link Type'),
                'values'   => $yesnoSource,
            )
        );
   }
   
   /**
    * 
    * @param Mage_Catalog_Model_Resource_Eav_Attribute $model
    */
   protected function _modelSaveCommitAfterResourceEavAttribute($model)
   {
       Mage::helper('customRelations/link')->createForAttributeIfNeeded($model);
   }
   
   /**
    * 
    * @param Mage_Catalog_Model_Resource_Eav_Attribute $model
    */
   protected function _modelDeleteBeforeResourceEavAttribute($model)
   {
       Mage::helper('customRelations/link')->deleteForAttributeIfNeeded($model);
   }
   
   /**
    * so they said...
    * @deprecated since 1.4.0.0-alpha2
    */
    protected function _decodeInput($encoded)
    {
        parse_str($encoded, $data);
        foreach ($data as $key=>$value) {
            parse_str(base64_decode($value), $data[$key]);
        }
        return $data;
    }
    
    /**
     * Prepare the product link type data before being saved
     * @param Varien_Object $observer
     */
    public function catalogProductPrepareSave($observer)
    {
        if ($product = $observer->getProduct()) {
            $links = Mage::app()->getRequest()->getPost('links');
            if ($links) {
                foreach ($links as $linkName => $linkData) {
                    if (strpos($linkName, "custom_") === 0) {
                        $product->setData($linkName . '_link_data', $this->_decodeInput($linkData));
                    }
                }
            }
        }
    }
    
    /**
     * Save the product link type data after the entity was saved
     * @param Varien_Observer $observer
     */
    public function catalogProductSaveAfter($observer)
    {
        if ($product = $observer->getProduct()) {
            $linkResource = Mage::getModel("catalog/product_link")->getResource();
            foreach ($product->getData() as $key => $data) {
                if (preg_match('(^(?P<link_type_code>custom_[a-zA-Z_0-9]+)_link_data$)', $key, $matches)) {
                    $linkTypeCode = $matches['link_type_code'];
                    $linkTypeId = Mage::helper('customRelations/link')->getProductLinkTypeIdByCode($linkTypeCode);
                    $linkResource->saveProductLinks($product, $data, $linkTypeId);
                }
            }
        }
    }

    /**
     * 
     * @param Varien_Object $observer
     */
    public function controllerActionLayoutGenerateBlocksAfter($observer)
    {
        $action = $observer->getAction();
        $event = $observer->getEvent();
        $layout = $observer->getLayout();
        
        if (in_array('catalog_product_view', $layout->getUpdate()->getHandles())) {
            /** @var Mage_Catalog_Model_Product $product */
            $product = Mage::registry('product');
            
            Mage::helper('customRelations/product_view')->addCustomRelationsBlocks($product);
        }
    }
    
    /**
     * Handles the event controller_action_predispatch_checkout_cart_add
     * @param Varien_Object $observer
     */
    public function addCustomRelatedProductToCart($observer)
    {
        $post = Mage::app()->getRequest()->getPost();
        $productsToAdd = array();
        foreach ($post as $varname => $value) {
            if (strpos($varname, 'select_custom_relation_list') === 0) {
                $productsToAdd[] = $value;
            }
        }
        
        if (count($productsToAdd)) {
            $cart = Mage::getSingleton('checkout/cart');
            $cart->addProductsByIds($productsToAdd);
        }
    }

    /**
     * Runs the import cronjob
     */
    public function importCron()
    {

        $import = Mage::helper('customRelations/import');
        $import->setVerboseMode(true);
        ob_start();
        $import->importCron();
        return ob_get_clean();
    }

    /**
     * Handles the event intellimage_customRelations_savecontrab
     * to save the crontab expression (if changed)
     */
    public function saveCrontabExpression($observer)
    {
        $configData = $observer->getConfigData();


        if ($configData->getPath() === 'system/customrelationsimport/daily_import_crontab') {
            $config = new Mage_Core_Model_Config();
            $config->saveConfig('crontab/jobs/customrelations_import/schedule/cron_expr', $configData->getValue(), 'default', 0);
        }

    }
    
}