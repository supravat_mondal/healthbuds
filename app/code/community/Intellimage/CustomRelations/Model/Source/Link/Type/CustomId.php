<?php
/**
 * Intellimage
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to mauricioprado00@gmail.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade your
 * Intellimage extension to newer versions in the future. 
 * If you wish to customize your Intellimage extension to your
 * needs please refer to mauricioprado00@gmail.com for more information.
 *
 * @package     Intellimage_CustomRelations
 * @author      Hugo Mauricio Prado Macat
 * @copyright   2013
 * @email       mauricioprado00@gmail.com
 * @license     http://www.opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */



class Intellimage_CustomRelations_Model_Source_Link_Type_CustomId
{

    public function toOptionArray()
    {
        $options = array();
        $collection = Mage::getModel('customRelations/product_link_type')->getCollection();

        foreach ($collection as $item) {
            $code = $item->getCode();

            if (strpos($code, 'custom_') !== 0) {
                continue;
            }

            $attribute = Mage::helper('customRelations/link')->getAttributeForLinkTypeCode($code);

            $options[] = array(
                'value' => $item->getId(),
                'code' => $code,
                'label' => $attribute->getFrontendLabel(),
            );
        }

        return $options;
    }

}
